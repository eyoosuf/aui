
module.exports = function(grunt){
    var files = grunt.option('files') ? [grunt.option('files')] : [ 'Gruntfile.js', 'src/js/{*,**/*}'];
    return {
        options: {
            jshintrc: '.jshintrc'
        },
        all: files
    }
};
