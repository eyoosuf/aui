module.exports = {
    dist: [
        'dist'
    ],
    tmp: [
        '.tmp'
    ],
    postDist: [
        'dist/*',
        '!dist/aui'
    ],
    deps: [
        'node_modules',
        'bower_components'
    ]
};
