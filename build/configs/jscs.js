module.exports = function (grunt) {
    'use strict';
    var files;
    if (grunt.option('files')) {
        files = [grunt.option('files')];
    } else {
        files = ['Gruntfile.js', 'src/js/{*,**/*}'];
    }

    return {
        all: files
    };
};
