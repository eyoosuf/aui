module.exports = {
    dist: {
        files: {
            'dist/aui/css/aui.min.css': ['dist/aui/css/aui.css'],
            'dist/aui/css/aui-experimental.min.css': ['dist/aui/css/aui-experimental.css']
        }
    }
};
