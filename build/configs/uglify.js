module.exports = {
    dist: {
        options: {
            beautify: {
                ascii_only: true,
                quote_keys: true
            }
        },
        files: {
            'dist/aui/js/aui-datepicker.min.js': ['<%= paths.dist %>aui/js/aui-datepicker.js'],
            'dist/aui/js/aui-css-deprecation-warnings.min.js': ['<%= paths.dist %>aui/js/aui-css-deprecation-warnings.js'],
            'dist/aui/js/aui-experimental.min.js': ['<%= paths.dist %>aui/js/aui-experimental.js'],
            'dist/aui/js/aui-soy.min.js': ['<%= paths.dist %>aui/js/aui-soy.js'],
            'dist/aui/js/aui.min.js': ['<%= paths.dist %>aui/js/aui.js']
        }
    }
};
