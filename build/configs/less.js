module.exports = {
    dist: {
        options: {
            paths: ['src/less'],
            ieCompat: true
        },
        files: {
            'dist/aui/css/aui.css': '<%= paths.styleSource %>/batch/main.less',
            'dist/aui/css/aui-experimental.css': '<%= paths.styleSource %>/batch/experimental.less'
        }
    }
};
