module.exports = function (grunt) {
    'use strict';

    var browsers = ['PhantomJS'];

    if (grunt && grunt.option && grunt.option('browsers')) {
        browsers = grunt.option('browsers');
        browsers = browsers.trim().replace(/[\s,]+/, ' ');
        browsers = browsers.split(' ');
    }

    var config = {
        frameworks: ['mocha', 'requirejs', 'sinon-chai'],
        browsers: browsers,
        reporters: ['coverage', 'progress', 'junit'],
        coverageReporter: {
            dir: 'reports/istanbul',
            type: 'html'
        },
        junitReporter: {
            outputFile: 'tests/karma.xml',
            suite: ''
        },
        preprocessors: {
            'src/less/{**/*,*}.less': 'less'
        },
        lessPreprocessor: {
            options: {
                paths: ['src/less/**'],
                save: false
            }
        },
        files: [
            // setup
            '.tmp/tests-requirejs-config.js',
            'tests/unit/karma-main.js',
            'tests/unit/polyfills.js',

            // src
            'src/less/batch/experimental.less',
            'src/less/batch/main.less',
            { pattern: 'bower_components/**/*.js', included: false },
            { pattern: 'node_modules/amd-loader-text/text.js', included: false },
            { pattern: 'src/**/*.js', included: false },

            // test
            'tests/unit/**/*.css',
            { pattern: '.tmp/**/*.js', included: false },
            { pattern: 'tests/**/*.js', included: false },
            { pattern: 'tests/**/*.html', included: false }
        ]
    };

    if (!grunt.option('debug')) {
        config.preprocessors['src/js/{**/*,*}.js'] = 'coverage';
    }

    return config;
};
