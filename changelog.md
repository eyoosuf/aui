# 5.9.0

[Documentation](https://docs.atlassian.com/aui/5.9.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.0)

## Highlights
* Deprecated AJS(), AJS.filterBySearch(), AJS.include(), AJS.setVisible(), AJS.setCurrent(), AJS.isVisible(). Use jQuery or native alternatives instead.
* Deprecated restful table helpers: AJS.triggerEvt(), AJS.bindEvt(), AJS.triggerEvtForInst(). These methods have been moved inside restful table.

# 5.8.0

[Documentation](https://docs.atlassian.com/aui/5.8.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.0)

## Upgrade Notes
* aui-ie9.css has been removed from the flatpack. It is no longer necessary to include this file.
* If you are using the sidebar from the flatpack, you will now need to include aui-experimental.js
* The contents of an AUI Dropdown2 can now be created entirely using Soy (instead of sending html to aui.dropdown2.contents)
* The markup generated using the AUI Dropdown2 Soy templates has changed. This new markup pattern is now much more accessible to screen readers.

# 5.7.12
[Documentation](https://docs.atlassian.com/aui/5.7.12/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.12)

# 5.7.11
[Documentation](https://docs.atlassian.com/aui/5.7.11/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.11)

# 5.7.10
[Documentation](https://docs.atlassian.com/aui/5.7.10/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.10)

# 5.7.9
[Documentation](https://docs.atlassian.com/aui/5.7.9/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.9)

# 5.7.8
[Documentation](https://docs.atlassian.com/aui/5.7.8/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.8)

# 5.7.7
[Documentation](https://docs.atlassian.com/aui/5.7.7/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.7)

## Highlights
* Fixed persistent flags to be dismissable. See [AUI-2893](https://ecosystem.atlassian.net/browse/AUI-2893)

## Upgrade Notes
**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Flags created with the previous 5.7.1 API will work exactly as they did previously in 5.7.7. Upgrading from these versions
is **non breaking** (existing calls to flag will behave exactly as they did). However, the "persistent" has been deprecated,
and the "close" option introduced instead.
* Flags with "persistent: true" should become "close: never"
* Flags with "persistent: false" should become "close: manual" (close: "manual" is the default, so you do not need to explicitly set this)

**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Upgrading from 5.7.4 **will change** behaviour of flags with "persistent: false" set. In 5.7.4, flags automatically faded
from view. They now require a user to dismiss them (they have close: "manual" behaviour)
* Flags with "persistent: true" should become "close: never". Flags with "persistent: true" maintain the same behaviour.
* Flags with "persistent: false" no longer fade after five seconds. If you wish them to, they should become "close: auto". If you do do not wish them to, they should become "close: manual"

#5.7.6
This is a broken release containing no bugfixes and should not be used. Please use the latest 5.7.x release instead

#5.7.5
[Documentation](https://docs.atlassian.com/aui/5.7.5/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.5)

#5.7.4
[Documentation](https://docs.atlassian.com/aui/5.7.4/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.4)

## Highlights
* Flags auto-close was ported over to this release from master.

#5.7.3
[Documentation](https://docs.atlassian.com/aui/5.7.3/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.3)

## Highlights
* New Confluence Icon Fonts
Note: normally we would do this in a minor version. However to avoid upgrade friction for Confluence we've determined it is acceptable to put these in a patch release.

#5.7.2
[Documentation](https://docs.atlassian.com/aui/5.7.2/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.2)

#5.7.1
[Documentation](https://docs.atlassian.com/aui/5.7.1/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.1)

#5.7.0
[Documentation](https://docs.atlassian.com/aui/5.7.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.0)

## Highlights
* Buttons have become flat and have had gradients removed
* Inset shadows have been removed from fields in forms
* Form notification is available as a new API (show tooltip messages on fields)
* Flag messages are available as a new API
* Inline Dialog 2 is available as a new API
* System notifications have been changed (no longer electric Charlie)
* The following components have no global variable and are only exposed via AMD modules:
  * Flags
  * Form Notification
  * Form Validation
  * Inline Dialog 2
* [Animated examples](https://developer.atlassian.com/display/AUI/AUI+5.7.0+Release+Notes) are available.

## Upgrade Notes
* If you have been using Form Validation, please change your data attributes
  * from: `<input data-aui-validate-... />`
  * to: `<input data-aui-validation-... />`
* If you are using Inline Dialog 2, Flags, Form Notification or Form Validation, you will need an AMD loader to initialise their behaviour on pages:
  * `require(['aui/inline-dialog2']); // Initialises all Inline Dialog 2s`
  * `require(['aui/flag']); // Initialises all flags`
  * `require(['aui/form-notification']); // Initialises all form notifications`
  * `require(['aui/form-validation', 'form-provided-validators']); // Loads and initialises form validations (and notifications) API. Also loads all AUI-provided form validators`

For all release notes older than 5.7.0, check [DAC](https://developer.atlassian.com/display/AUI/AUI+release+notes)
