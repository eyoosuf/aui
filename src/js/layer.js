;(function(init) {
    'use strict';

    AJS.layer = init(AJS.$, AJS._internal.widget);

    if (typeof define === 'function') {
        define(function () {
            return AJS.layer;
        });
    }
})(function($, widget) {
    'use strict';

    var EVENT_PREFIX = '_aui-internal-layer-';
    var GLOBAL_EVENT_PREFIX = '_aui-internal-layer-global-';
    var LAYER_EVENT_PREFIX = 'aui-layer-';
    var $doc = $(document);

    function ariaHide ($el) {
        $el.attr('aria-hidden', 'true');
    }

    function ariaShow ($el) {
        $el.attr('aria-hidden', 'false');
    }

    function triggerEvent ($el, deprecatedName, newNativeName) {
        var e1 = $.Event(EVENT_PREFIX + deprecatedName);
        var e2 = $.Event(GLOBAL_EVENT_PREFIX + deprecatedName);
        var nativeEvent = new CustomEvent(LAYER_EVENT_PREFIX + newNativeName, {
            bubbles: true,
            cancelable: true
        });

        $el.trigger(e1);
        $el.trigger(e2, [$el]);
        $el[0].dispatchEvent(nativeEvent);

        return !e1.isDefaultPrevented() && !e2.isDefaultPrevented() && !nativeEvent.defaultPrevented;
    }


    function Layer (selector) {
        this.$el = $(selector || '<div class="aui-layer" aria-hidden="true"></div>');
        this.$el.addClass('aui-layer');
    }

    Layer.prototype = {
        /**
         * Returns the layer below the current layer if it exists.
         *
         * @returns {jQuery | undefined}
         */
        below: function () {
            return AJS.LayerManager.global.item(AJS.LayerManager.global.indexOf(this.$el) - 1);
        },

        /**
         * Returns the layer above the current layer if it exists.
         *
         * @returns {jQuery | undefined}
         */
        above: function () {
            return AJS.LayerManager.global.item(AJS.LayerManager.global.indexOf(this.$el) + 1);
        },

        /**
         * Sets the width and height of the layer.
         *
         * @param {Integer} width The width to set.
         * @param {Integer} height The height to set.
         *
         * @returns {Layer}
         */
        changeSize: function (width, height) {
            this.$el.css('width', width);
            this.$el.css('height', height === 'content' ? '' : height);
            return this;
        },

        /**
         * Binds a layer event.
         *
         * @param {String} event The event name to listen to.
         * @param {Function} fn The event handler.
         *
         * @returns {Layer}
         */
        on: function (event, fn) {
            this.$el.on(EVENT_PREFIX + event, fn);
            return this;
        },


        /**
         * Unbinds a layer event.
         *
         * @param {String} event The event name to unbind=.
         * @param {Function} fn Optional. The event handler.
         *
         * @returns {Layer}
         */
        off: function (event, fn) {
            this.$el.off(EVENT_PREFIX + event, fn);
            return this;
        },

        /**
         * Shows the layer.
         *
         * @returns {Layer}
         */
        show: function () {
            if (this.isVisible()) {
                ariaShow(this.$el);
                return this;
            }

            if (triggerEvent(this.$el, 'beforeShow', 'show')) {
                AJS.LayerManager.global.push(this.$el);
            }

            return this;
        },

        /**
         * Hides the layer.
         *
         * @returns {Layer}
         */
        hide: function () {
            if (!this.isVisible()) {
                ariaHide(this.$el);
                return this;
            }

            if (triggerEvent(this.$el, 'beforeHide', 'hide')) {
                AJS.LayerManager.global.popUntil(this.$el);
            }

            return this;
        },

        /**
         * Checks to see if the layer is visible.
         *
         * @returns {Boolean}
         */
        isVisible: function () {
            return this.$el.attr('aria-hidden') === 'false';
        },

        /**
         * Removes the layer and cleans up internal state.
         *
         * @returns {undefined}
         */
        remove: function () {
            this.hide();
            this.$el.remove();
            this.$el = null;
        },

        /**
         * Returns whether or not the layer is blanketed.
         *
         * @returns {Boolean}
         */
        isBlanketed: function () {
            return this.$el.attr('data-aui-blanketed') === 'true';
        },

        /**
         * Returns whether or not the layer is persistent.
         *
         * @returns {Boolean}
         */
        isPersistent: function () {
            var modal = this.$el.attr('modal') || this.$el.attr('data-aui-modal');
            var persistent = this.$el.attr('persistent') || this.$el.attr('data-aui-persistent');

            return modal === 'true' || persistent === 'true';
        },

        _hideLayer: function (triggerBeforeEvents) {
            if (this.isPersistent() || this.isBlanketed()) {
                AJS.FocusManager.global.exit(this.$el);
            }

            if (triggerBeforeEvents) {
                triggerEvent(this.$el, 'beforeHide', 'hide');
            }

            this.$el.attr('aria-hidden', 'true');
            this.$el.css('z-index', this.$el.data('_aui-layer-cached-z-index') || '');
            this.$el.data('_aui-layer-cached-z-index', '');
            this.$el.trigger(EVENT_PREFIX + 'hide');
            this.$el.trigger(GLOBAL_EVENT_PREFIX + 'hide', [this.$el]);
        },

        _showLayer: function (zIndex) {
            if (!this.$el.parent().is('body')) {
                this.$el.appendTo(document.body);
            }

            this.$el.data('_aui-layer-cached-z-index', this.$el.css('z-index'));
            this.$el.css('z-index', zIndex);
            this.$el.attr('aria-hidden', 'false');

            if (this.isPersistent() || this.isBlanketed()) {
                AJS.FocusManager.global.enter(this.$el);
            }

            this.$el.trigger(EVENT_PREFIX + 'show');
            this.$el.trigger(GLOBAL_EVENT_PREFIX + 'show', [this.$el]);
        }
    };

    var layerWidget = widget('layer', Layer);

    layerWidget.on = function (eventName, selector, fn) {
        $doc.on(GLOBAL_EVENT_PREFIX + eventName, selector, fn);
        return this;
    };

    layerWidget.off = function (eventName, selector, fn) {
        $doc.off(GLOBAL_EVENT_PREFIX + eventName, selector, fn);
        return this;
    };


    return layerWidget;
});
