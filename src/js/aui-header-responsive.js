// Self executing function so we can pass in jquery.
(function ($, skate, template) {
    'use strict';

    var $window = $(window);

    function Header (element) {
        var that = this;

        this.element = element;
        this.$element = $(element);
        this.index = $('aui-header, .aui-header').index(element);
        this.secondaryNav = this.$element.find('.aui-header-secondary .aui-nav').first();
        this.menuItems = [];
        this.availableWidth = 0;
        this.totalWidth = 0;
        this.moreMenu = undefined;
        this.previousIndex = undefined;
        this.applicationLogo = this.$element.find('#logo');
        this.moreMenuWidth = 0;
        this.primaryButtonsWidth = 0;

        // to cache the selector and give .find convenience
        this.inHeader = (function () {
            var header = that.$element.find('.aui-header-primary').first();

            return function (selector) {
                return header.find(selector);
            };
        })();
    }

    Header.prototype = {
        init: function () {
            var that = this;

            this.element.setAttribute('data-aui-responsive', 'true');
            this.inHeader('.aui-button').parent().each(function () {
                that.primaryButtonsWidth += $(this).outerWidth(true);
            });

            // remember the widths of all the menu items
            this.inHeader('.aui-nav > li > a:not(.aui-button)').each(function () {
                var $this = $(this).parent();
                var itemWidth = $this.outerWidth(true);

                that.totalWidth += itemWidth;
                that.menuItems.push({
                    itemElement: $this,
                    itemWidth: itemWidth
                });
            });

            this.previousIndex = this.menuItems.length;

            // attach resize handler
            $window.resize(function () {
                that.previousIndex = that.constructResponsiveDropdown();
            });

            // create the elements for the show more menu
            this.createResponsiveDropdownTrigger();

            // So that the header logo doesn't mess things up. (size is unknown before the image loads)
            var $logoImg = this.applicationLogo.find('img');

            if ($logoImg.length !== 0) {
                $logoImg.attr('data-aui-responsive-header-index', this.index);
                $logoImg.load(function () {
                    that.previousIndex = that.constructResponsiveDropdown();
                });
            }

            // construct the show more dropdown
            this.previousIndex = this.constructResponsiveDropdown();

            // show the aui nav (hidden via css on load)
            this.inHeader('.aui-nav').css('width', 'auto');
        },

        // calculate widths based on the current state of the page
        calculateAvailableWidth: function () {
            // if there is no secondary nav, use the right of the screen as the boundary instead
            var rightMostBoundary = this.secondaryNav.length !== 0 ? this.secondaryNav.offset().left : this.$element.outerWidth();

            // the right most side of the primary nav, this is assumed to exists if this code is running
            var primaryNavRight = this.applicationLogo.offset().left + this.applicationLogo.outerWidth(true) + this.primaryButtonsWidth;

            return rightMostBoundary - primaryNavRight;
        },

        constructResponsiveDropdown: function () {
            var remaining;
            var availableWidth = this.calculateAvailableWidth(this.$element, this.primaryButtonsWidth);

            if (availableWidth > this.totalWidth) {
                this.showAll();
            } else {
                this.moreMenu.show();
                remaining = availableWidth - this.moreMenuWidth;

                // loop through menu items until no more remaining space
                // i represents the index of the last item in the header
                for (var i = 0; remaining >= 0; i++) {
                    remaining -= this.menuItems[i].itemWidth;
                }

                // Subtract one for fencepost
                --i;

                // move everything after the last index into the show more dropdown
                this.moveToResponsiveDropdown(i);

                // move everything between the previous index and the current index out of the dropdown
                this.moveOutOfResponsiveDropdown(i);

                // return the index of the last last item in the header so we can remember it for next time
                return i;
            }
        },

        // creates the trigger and content elements for the show more dropdown
        createResponsiveDropdownTrigger: function () {
            var item = document.createElement('li');
            var dropdown = document.createElement('aui-dropdown');

            dropdown.id = 'aui-responsive-header-dropdown-' + this.index;
            template.wrap(dropdown).innerHTML = '<ul id="aui-responsive-header-dropdown-list-' + this.index + '"></ul>';
            dropdown.setAttribute('label', AJS.I18n.getText('aui.words.more'));
            item.appendChild(dropdown);

            // detect if buttons exist
            if (this.primaryButtonsWidth === 0) {
                this.inHeader('.aui-nav').append(item);
            } else {
                this.inHeader('.aui-nav > li > .aui-button').first().parent().before(item);
            }

            this.moreMenu = $(item);
            this.moreMenuWidth = this.moreMenu.outerWidth(true);
        },

        // function that handles moving items out of the show more menu into the app header
        moveOutOfResponsiveDropdown: function (index) {
            if (index < 0 || this.previousIndex < 0 || index === this.previousIndex) {
                return;
            }

            var $responsiveTrigger = $('#aui-responsive-header-dropdown-' + this.index);
            var $responsiveTriggerItem = $responsiveTrigger.parent();
            var current;
            var currentItem;

            if ($responsiveTrigger.hasClass('active')) {
                $responsiveTrigger.trigger('aui-button-invoke');
            }

            var menuItemElementsLength = this.inHeader('.aui-nav > li > aui-dropdown:not(#aui-responsive-header-dropdown-' + this.index + ')').length;

            while (index > this.previousIndex) {
                current = this.menuItems[this.previousIndex];

                // Make sure things exist before accessing them.
                if (current && current.itemElement) {
                    currentItem = current.itemElement;

                    if (menuItemElementsLength === 0) {
                        // this path should only run once when there are no menu items left in the header
                        currentItem.prependTo(this.inHeader('.aui-nav'));
                    } else {
                        currentItem.insertBefore($responsiveTriggerItem);
                    }

                    currentItem.children('a').removeClass('aui-dropdown2-sub-trigger active');
                    this.previousIndex = this.previousIndex + 1;
                    menuItemElementsLength = menuItemElementsLength + 1;
                }
            }
        },

        // function that handles moving itesm into the show more menu
        moveToResponsiveDropdown: function (index) {
            if (index < 0) {
                return;
            }

            var dropdownContainer = $('#aui-responsive-header-dropdown-list-' + this.index);

            for (var i = index; i < this.menuItems.length; i++) {
                this.menuItems[i].itemElement.appendTo(dropdownContainer);

                var $itemTrigger = this.menuItems[i].itemElement.children('a');

                if ($itemTrigger.hasClass('aui-dropdown2-trigger')) {
                    $itemTrigger.addClass('aui-dropdown2-sub-trigger');
                }
            }
        },

        // function that handles show everything
        showAll: function () {
            this.moreMenu.hide();
            this.moveOutOfResponsiveDropdown(this.menuItems.length, this.previousIndex);
        }
    };

    function createHeader (element) {
        var header = new Header(element);
        header.init();
    }

    function findAndCreateHeaders () {
        $('.aui-header').each(function () {
            createHeader(this);
        });
    }

    $(findAndCreateHeaders);

    AJS.responsiveheader = {};
    AJS.responsiveheader.setup = AJS.deprecate.fn(findAndCreateHeaders, 'AJS.responsiveheader.setup', {
        removeInVersion: '6.0.0',
        sinceVersion: '5.8.0',
        extraInfo: 'No need to manually initialise anymore as this is now a web component.'
    });

    skate('aui-header', {
        type: skate.types.TAG,

        created: function (element) {
            $(element).find('.aui-banner').addClass('aui-banner-error');
        },

        attached: function (element) {
            createHeader(element);
        },

        attributes: {
            link: function (element, data) {
                element.querySelector('#logo > a').setAttribute('href', data.newValue);
            },

            responsive: function (element, data) {
                element.querySelector('.aui-header').setAttribute('data-aui-responsive', data.newValue);
            }
        },

        template: template(
            '<content select="aui-banner"></content>',
            '<nav class="aui-header aui-dropdown2-trigger-group" role="navigation">',
                '<content select=".aui-header-before"></content>',
                '<div class="aui-header-primary">',
                    '<h1 id="logo" class="aui-header-logo">',
                        '<a href="/">',
                            '<content select=".aui-header-logo, .aui-header-logo-device, .aui-header-logo-text"></content>',
                        '</a>',
                    '</h1>',
                    '<content select=".aui-header-content"></content>',
                '</div>',
                '<content select=".aui-header-secondary"></content>',
                '<content select=".aui-header-after"></content>',
            '</nav>'
        )
    });
})(AJS.$, window.skate, window.skateTemplateHtml);
