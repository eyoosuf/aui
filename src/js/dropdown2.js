(function($, Alignment, skate, template, browser) {
    'use strict';

    function isChecked(el) {
        return $(el).is('.checked, .aui-dropdown2-checked, [aria-checked="true"]');
    }

    function getTrigger(control) {
        return $('[aria-controls="' + control.id + '"]')[0];
    }

    function doIfTrigger(triggerable, callback) {
        var trigger = getTrigger(triggerable);

        if (trigger) {
            callback(trigger);
        }
    }

    function setDropdownTriggerActiveState(trigger, isActive) {
        var $trigger = $(trigger);

        if (isActive) {
            $trigger.attr('aria-expanded', 'true');
            $trigger.addClass('active aui-dropdown2-active');
        } else {
            $trigger.attr('aria-expanded', 'false');
            $trigger.removeClass('active aui-dropdown2-active');
        }
    }


    // The dropdown's trigger
    // ----------------------

    function triggerCreated (trigger) {
        var dropdownID = trigger.getAttribute('aria-controls');

        if (!dropdownID) {
            dropdownID = trigger.getAttribute('aria-owns');

            if (!dropdownID) {
                AJS.error('Dropdown triggers need either a "aria-owns" or "aria-controls" attribute');
            } else {
                trigger.removeAttribute('aria-owns');
                trigger.setAttribute('aria-controls', dropdownID);
            }
        }

        trigger.setAttribute('aria-haspopup', true);
        trigger.setAttribute('aria-expanded', false);
        trigger.setAttribute('href', '#');

        function handleIt(e) {
            e.preventDefault();

            if (!trigger.isEnabled()) {
                return;
            }

            var dropdown = document.getElementById(dropdownID);
            dropdown.toggle();
            dropdown.isSubmenu = trigger.hasSubmenu();

            return dropdown;
        }

        function handleOpen(e) {
            e.preventDefault();

            if (!trigger.isEnabled() || !trigger.hasSubmenu()) {
                return;
            }

            var dropdown = document.getElementById(dropdownID);
            dropdown.show();
            dropdown.isSubmenu = trigger.hasSubmenu();

            return dropdown;
        }

        function handleKeydown(e) {
            var normalInvoke = (e.keyCode === AJS.keyCode.ENTER || e.keyCode === AJS.keyCode.SPACE);
            var submenuInvoke = (e.keyCode === AJS.keyCode.RIGHT && trigger.hasSubmenu());
            var rootMenuInvoke = ((e.keyCode === AJS.keyCode.UP || e.keyCode === AJS.keyCode.DOWN) && !trigger.hasSubmenu());

            if (normalInvoke || submenuInvoke || rootMenuInvoke) {
                var dropdown = handleIt(e);

                if (dropdown) {
                    dropdown.focusItem(0);
                }
            }
        }

        $(trigger)
            .on('aui-button-invoke', handleIt)
            .on('click', handleIt)
            .on('keydown', handleKeydown)
            .on('mouseenter', handleOpen)
        ;
    }

    var triggerPrototype = {
        disable: function() {
            this.setAttribute('aria-disabled', 'true');
        },

        enable: function() {
            this.setAttribute('aria-disabled', 'false');
        },

        isEnabled: function() {
            return this.getAttribute('aria-disabled') !== 'true';
        },

        hasSubmenu: function() {
            var triggerClasses = (this.className || '').split(/\s+/);
            return triggerClasses.indexOf('aui-dropdown2-sub-trigger') !== -1;
        }
    };

    skate('aui-dropdown2-trigger', {
        type: skate.types.CLASS,
        created: triggerCreated,
        prototype: triggerPrototype
    });

    //To remove at a later date. Some dropdown triggers initialise lazily, so we need to listen for mousedown
    //and synchornously init before the click event is fired.
    //TODO: delete in AUI 6.0.0, see AUI-2868
    function bindLazyTriggerInitialisation() {
        $(document).on('mousedown', '.aui-dropdown2-trigger', function () {
            var isElementSkated = this.hasAttribute('resolved');
            if (!isElementSkated) {
                skate.init(this);
                var lazyDeprecate = AJS.deprecate.getMessageLogger('Dropdown2 lazy initialisation', {
                    removeInVersion: '6.0.0',
                    alternativeName: 'initialisation on DOM insertion',
                    sinceVersion: '5.8.0',
                    extraInfo: 'Dropdown2 triggers should have all necessary attributes on DOM insertion',
                    deprecationType: 'JS'
                });
                lazyDeprecate();
            }
        });
    }

    bindLazyTriggerInitialisation();

    skate('aui-dropdown2-sub-trigger', {
        type: skate.types.CLASS,
        created: function(trigger) {
            trigger.className += ' aui-dropdown2-trigger';
            skate.init(trigger);
        }
    });


    // Dropdown trigger groups
    // -----------------------

    $(document).on('mouseenter', '.aui-dropdown2-trigger-group a, .aui-dropdown2-trigger-group button', function(e) {
        var $item = $(e.target);
        var groupHasOpenDropdown;

        if ($item.is('.aui-dropdown2-active')) {
            return; // No point doing anything if we're hovering over the already-active item trigger.
        }

        if ($item.closest('.aui-dropdown2').size()) {
            return; // We don't want to deal with dropdown items, just the potential triggers in the group.
        }

        groupHasOpenDropdown = $item.closest('.aui-dropdown2-trigger-group').find('.aui-dropdown2-active').size();

        if (groupHasOpenDropdown && $item.is('.aui-dropdown2-trigger')) {
            $item.trigger('aui-button-invoke'); // Open this trigger's menu.
            e.preventDefault();
        }
    });


    // Dropdown items
    // --------------

    function getDropdownItems (dropdown, filter) {
        return $(dropdown)
            .find('> ul > li, > .aui-dropdown2-section > ul > li, > div > .aui-dropdown2-section > div[role="group"] > ul > li')
            .filter(filter)
            .children('a, button, [role="checkbox"], [role="menuitemcheckbox"], [role="radio"], [role="menuitemradio"]');
    }

    function getAllDropdownItems (dropdown) {
        return getDropdownItems(dropdown, function() { return true;});
    }

    function getVisibleDropdownItems (dropdown) {
        return getDropdownItems(dropdown, function() { return this.className.indexOf('hidden') === -1; });
    }

    function amendDropdownItem (item) {
        var $item = $(item);

        $item.attr('tabindex', '-1');

        /**
         * Honouring the documentation.
         * @link https://docs.atlassian.com/aui/latest/docs/dropdown2.html
         */
        if ($item.hasClass('aui-dropdown2-disabled') || $item.parent().hasClass('aui-dropdown2-hidden')) {
            $item.attr('aria-disabled', true);
        }
    }

    function amendDropdownContent (dropdown) {
        // Add assistive semantics to each dropdown item
        getAllDropdownItems(dropdown).each(function() {
            amendDropdownItem(this);
        });
    }

    /**
     * Honours behaviour for code written using only the legacy class names.
     * To maintain old behaviour (i.e., remove the 'hidden' class and the item will become un-hidden)
     * whilst allowing our code to only depend on the new classes, we need to
     * keep the state of the DOM in sync with legacy classes.
     *
     * Calling this function will add the new namespaced classes to elements with legacy names.
     * @returns {Function} a function to remove the new namespaced classes, only from the elements they were added to.
     */
    function migrateAndSyncLegacyClassNames (dropdown) {
        var $dropdown = $(dropdown);

        // Migrate away from legacy class names
        var $hiddens = $dropdown.find('.hidden').addClass('aui-dropdown2-hidden');
        var $disableds = $dropdown.find('.disabled').addClass('aui-dropdown2-disabled');
        var $interactives = $dropdown.find('.interactive').addClass('aui-dropdown2-interactive');

        return function revertToOriginalMarkup() {
            $hiddens.removeClass('aui-dropdown2-hidden');
            $disableds.removeClass('aui-dropdown2-disabled');
            $interactives.removeClass('aui-dropdown2-interactive');
        };
    }


    // The Dropdown itself
    // -------------------

    function setLayerAlignment(dropdown, trigger) {
        var alignment = dropdown.getAttribute('data-aui-alignment');

        if (!alignment) {
            alignment = 'bottom auto';

            if (trigger && trigger.hasSubmenu && trigger.hasSubmenu()) {
                alignment = 'submenu auto';
            }

            dropdown.setAttribute('data-aui-alignment', alignment);
            dropdown.setAttribute('data-aui-alignment-static', true);
        }
    }

    function getDropdownHideLocation(dropdown, trigger) {
        var possibleHome = trigger.getAttribute('data-dropdown2-hide-location');
        return document.getElementById(possibleHome) || dropdown.parentNode;
    }

    function bindDropdownBehaviourToLayer(dropdown) {
        AJS.layer(dropdown);

        dropdown.addEventListener('aui-layer-show', function() {
            $(dropdown).trigger('aui-dropdown2-show');

            dropdown._syncClasses = migrateAndSyncLegacyClassNames(dropdown);

            amendDropdownContent(this);
            doIfTrigger(dropdown, function(trigger) {
                setDropdownTriggerActiveState(trigger, true);
                setLayerAlignment(dropdown, trigger);

                dropdown._returnTo = getDropdownHideLocation(dropdown, trigger);

                if (dropdown._auiAlignment) {
                    dropdown._auiAlignment.destroy();
                }

                dropdown._auiAlignment = new Alignment(dropdown, trigger);

                dropdown._auiAlignment.enable();
            });
        });

        dropdown.addEventListener('aui-layer-hide', function() {
            $(dropdown).trigger('aui-dropdown2-hide');

            if (dropdown._syncClasses) {
                dropdown._syncClasses();
                delete dropdown._syncClasses;
            }

            if (dropdown._auiAlignment) {
                dropdown._auiAlignment.disable();
                dropdown._auiAlignment.destroy();
            }

            if (dropdown._returnTo) {
                if (dropdown.parentNode && dropdown.parentNode !== dropdown._returnTo) {
                    dropdown.parentNode.removeChild(dropdown);
                }
                dropdown._returnTo.appendChild(dropdown);
            }

            getVisibleDropdownItems(dropdown).removeClass('active aui-dropdown2-active');

            doIfTrigger(dropdown, function(trigger) {
                if (wasProbablyClosedViaKeyboard()) {
                    trigger.focus();
                    setDropdownTriggerActiveState(trigger, trigger.hasSubmenu && trigger.hasSubmenu());
                } else {
                    setDropdownTriggerActiveState(trigger, false);
                }
            });

            // Gets set by submenu trigger invocation. Bad coupling point?
            delete dropdown.isSubmenu;
        });
    }

    var keyboardClose = false;
    function keyboardCloseDetected () {
        keyboardClose = true;
    }

    function wasProbablyClosedViaKeyboard () {
        var result = (keyboardClose === true);
        keyboardClose = false;
        return result;
    }

    function bindItemInteractionBehaviourToDropdown (dropdown) {
        var $dropdown = $(dropdown);

        $dropdown.on('keydown', function(e) {
            if (e.keyCode === AJS.keyCode.DOWN) {
                dropdown.focusNext();
                e.preventDefault();
            } else if (e.keyCode === AJS.keyCode.UP) {
                dropdown.focusPrevious();
                e.preventDefault();
            } else if (e.keyCode === AJS.keyCode.LEFT) {
                if (dropdown.isSubmenu) {
                    keyboardCloseDetected();
                    dropdown.hide();
                    e.preventDefault();
                }
            } else if (e.keyCode === AJS.keyCode.ESCAPE) {
                // The closing will be handled by the LayerManager!
                keyboardCloseDetected();
            } else if (e.keyCode === AJS.keyCode.TAB) {
                keyboardCloseDetected();
                dropdown.hide();
            }
        });

        // close the menu when clicking on elements which aren't "interactive"
        $dropdown.on('click', 'a, button, [role="menuitem"], [role="menuitemcheckbox"], [role="checkbox"], [role="menuitemradio"], [role="radio"]', function(e) {
            var $item = $(e.target);

            if ($item.attr('aria-disabled') === 'true') {
                e.preventDefault();
            }

            if (!e.isDefaultPrevented() && !$item.is('.aui-dropdown2-interactive')) {
                var theMenu = dropdown;
                do {
                    var dd = AJS.layer(theMenu);
                    theMenu = AJS.layer(theMenu).below();
                    if (dd.$el.is('.aui-dropdown2')) {
                        dd.hide();
                    }
                } while (theMenu);
            }
        });

        // close a submenus when the mouse moves over items other than its trigger
        $dropdown.on('mouseenter', 'a, button, [role="menuitem"], [role="menuitemcheckbox"], [role="checkbox"], [role="menuitemradio"], [role="radio"]', function(e) {
            var item = e.target;
            var hasSubmenu = item.hasSubmenu && item.hasSubmenu();

            if (!e.isDefaultPrevented() && !hasSubmenu) {
                var maybeALayer = AJS.layer(dropdown).above();

                if (maybeALayer) {
                    AJS.layer(maybeALayer).hide();
                }
            }
        });
    }


    // Dropdowns
    // ---------

    function dropdownCreated (dropdown) {
        var $dropdown = $(dropdown);

        $dropdown.addClass('aui-dropdown2');

        // swap the inner div to presentation as application is only needed for Windows
        if (browser.supportsVoiceOver()) {
            $dropdown.find('> div[role="application"]').attr('role', 'presentation');
        }

        if (dropdown.hasAttribute('data-container')) {
            $dropdown.attr('data-aui-alignment-container', $dropdown.attr('data-container'));
            $dropdown.removeAttr('data-container');
        }

        bindDropdownBehaviourToLayer(dropdown);
        bindItemInteractionBehaviourToDropdown(dropdown);
        dropdown.hide();
    }

    var dropdownPrototype = {
        /**
         * Toggles the visibility of the dropdown menu
         *
         * @returns {undefined}
         */
        toggle: function() {
            if (this.isVisible()) {
                this.hide();
            } else {
                this.show();
            }
        },

        /**
         * Explicitly shows the menu
         *
         * @returns {HTMLElement}
         */
        show: function() {
            AJS.layer(this).show();
            return this;
        },

        /**
         * Explicitly hides the menu
         *
         * @returns {HTMLElement}
         */
        hide: function() {
            AJS.layer(this).hide();
            return this;
        },

        /**
         * Shifts explicit focus to the next available item in the menu
         *
         * @returns {undefined}
         */
        focusNext: function() {
            var $items = getVisibleDropdownItems(this);
            var selected = document.activeElement;
            var idx;

            if ($items.last()[0] !== selected) {
                idx = $items.toArray().indexOf(selected);
                this.focusItem($items.get(idx+1));
            }
        },

        /**
         * Shifts explicit focus to the previous available item in the menu
         *
         * @returns {undefined}
         */
        focusPrevious: function() {
            var $items = getVisibleDropdownItems(this);
            var selected = document.activeElement;
            var idx;

            if ($items.first()[0] !== selected) {
                idx = $items.toArray().indexOf(selected);
                this.focusItem($items.get(idx-1));
            }
        },

        /**
         * Shifts explicit focus to the menu item matching the index param
         *
         * @param {number} message The message to act on.
         *
         * @returns {undefined}
         */
        focusItem: function(item) {
            var $items = getVisibleDropdownItems(this);
            var $item;
            if (typeof item === 'number') {
                item = $items.get(item);
            }
            $item = $(item);
            $item.focus();
            $items.removeClass('active aui-dropdown2-active');
            $item.addClass('active aui-dropdown2-active');
        },

        /**
         * Checks whether or not the menu is currently displayed
         *
         * @returns {Boolean}
         */
        isVisible: function() {
            return AJS.layer(this).isVisible();
        }
    };

    skate('aui-dropdown', {
        attributes: {
            label: function (element, data) {
                element.children[0].textContent = data.newValue;
            }
        },

        created: function (element) {
            var dropdown = element.children[1];
            var trigger = element.children[0];

            dropdown.id = element.id + '-dropdown';
            trigger.id = element.id + '-trigger';
            trigger.setAttribute('aria-controls', dropdown.id);
            trigger.setAttribute('aria-owns', dropdown.id);

            // Rebind all prototype methods to use the dropdown element. This
            // can be removed once the old markup pattern is removed as it's
            // only here to share code.
            Object.keys(dropdownPrototype).forEach(function (key) {
                element[key] = element[key].bind(dropdown);
            });

            bindDropdownBehaviourToLayer(dropdown);
            bindItemInteractionBehaviourToDropdown(dropdown);
            element.hide();
        },

        prototype: dropdownPrototype,

        template: template(
            '<a class="aui-dropdown2-trigger" href="#"></a>',
            '<div aria-hidden="true" class="aui-dropdown2 aui-style-default" role="menu">',
                '<div role="application">',
                    '<content select="ul"></content>',
                '</div>',
            '</div>'
        ),

        type: skate.types.TAG,
    });

    skate('aui-dropdown2', {
        type: skate.types.CLASS,
        created: dropdownCreated,
        prototype: dropdownPrototype
    });

    skate('data-aui-dropdown2', {
        type: skate.types.ATTR,
        created: dropdownCreated,
        prototype: dropdownPrototype
    });


    // Checkboxes and radios
    // ---------------------

    skate('aui-dropdown2-checkbox', {
        type: skate.types.CLASS,

        created: function(checkbox) {
            var checked = isChecked(checkbox);
            checkbox.setAttribute('aria-checked', checked);
            checkbox.setAttribute('tabindex', '0');

            // swap from menuitemcheckbox to just plain checkbox for VoiceOver
            if (browser.supportsVoiceOver()) {
                checkbox.setAttribute('role','checkbox');
            }

            $(checkbox).on('click keydown', function(e) {
                if (e.type === 'click' || e.keyCode === AJS.keyCode.ENTER || e.keyCode === AJS.keycode.SPACE) {
                    if (checkbox.isInteractive()) {
                        e.preventDefault();
                    }

                    if (checkbox.isDisabled() === false) {
                        // toggle the checked state
                        if (checkbox.isChecked()) {
                            checkbox.uncheck();
                        } else {
                            checkbox.check();
                        }
                    }
                }
            });
        },

        prototype: {
            isDisabled: function() {
                return this.getAttribute('aria-disabled') !== null && this.getAttribute('aria-disabled') === 'true';
            },

            isChecked: function() {
                return this.getAttribute('aria-checked') !== null && this.getAttribute('aria-checked') === 'true';
            },

            isInteractive: function() {
                return $(this).hasClass('aui-dropdown2-interactive');
            },

            uncheck: function() {
                this.setAttribute('aria-checked', 'false');
                $(this).removeClass('checked aui-dropdown2-checked');
                $(this).trigger('aui-dropdown2-item-uncheck');
            },

            check: function() {
                this.setAttribute('aria-checked', 'true');
                $(this).addClass('checked aui-dropdown2-checked');
                $(this).trigger('aui-dropdown2-item-check');
            }
        }
    });

    skate('aui-dropdown2-radio', {
        type: skate.types.CLASS,

        created: function(radio) {
            // add a dash of ARIA
            var checked = isChecked(radio);
            radio.setAttribute('aria-checked', checked);
            radio.setAttribute('tabindex', '0');

            // swap from menuitemradio to just plain radio for VoiceOver
            if (browser.supportsVoiceOver()) {
                radio.setAttribute('role','radio');
            }

            $(radio).on('click keydown', function(e){
                if (e.type === 'click' || e.keyCode === AJS.keyCode.ENTER || e.keyCode === AJS.keycode.SPACE) {
                    if (radio.isInteractive()) {
                        e.preventDefault();
                    }
                    var $radio = $(this);

                    if (this.isDisabled() === false && this.isChecked() === false) {
                        // toggle the checked state
                        $radio.closest('ul').find('.aui-dropdown2-checked').not(this).each(function(){
                            this.uncheck();
                        });
                        radio.check();
                    }
                }
            });
        },

        prototype: {
            isDisabled: function() {
                return this.getAttribute('aria-disabled') !== null && this.getAttribute('aria-disabled') === 'true';
            },

            isChecked: function() {
                return this.getAttribute('aria-checked') !== null && this.getAttribute('aria-checked') === 'true';
            },

            isInteractive: function() {
                return $(this).hasClass('aui-dropdown2-interactive');
            },

            uncheck: function() {
                this.setAttribute('aria-checked', 'false');
                $(this).removeClass('checked aui-dropdown2-checked');
                $(this).trigger('aui-dropdown2-item-uncheck');
            },

            check: function() {
                this.setAttribute('aria-checked', 'true');
                $(this).addClass('checked aui-dropdown2-checked');
                $(this).trigger('aui-dropdown2-item-check');
            }
        }
    });
}(AJS.$, AJS.Alignment, window.skate, window.skateTemplateHtml, AJS._internal.browser));
