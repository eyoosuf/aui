define(['aui/internal/spin'], function (Spin) {
    'use strict';

    function Spinner (element) {
        var $ = AJS.$;
        this.$element = $(element);
        var spinContainer = document.createElement('div');
        var $spinContainer = $(spinContainer);
        $spinContainer.addClass('aui-button-spinner');

        var presets = {color: this.$element.css('color'), lines: 12, length: 3, width: 2, radius: 3, trail: 60, speed: 1.5};
        $spinContainer.data().spinner = new Spin(presets).spin(spinContainer);

        this.$element.append(spinContainer);
    }

    Spinner.prototype = {
        /**
         * Remove the spinner from the original element.
         */
        remove: function () {
            var $spinContainer = this.$element.children('.aui-button-spinner').first();

            var data = $spinContainer.data();
            if (data) {
                data.spinner.stop();
                delete data.spinner;
            }

            $spinContainer.remove();
        },

        /**
         * Show the spinner.
         */
        show: function () {
            var $spinContainer = this.$element.children('.aui-button-spinner').first();

            $spinContainer.css('display', 'block');
        },

        /**
         * Hide the spinner.
         */
        hide: function () {
            var $spinContainer = this.$element.children('.aui-button-spinner').first();

            $spinContainer.css('display', 'none');
        }
    };

    return Spinner;
});
