define(function () {
    'use strict';

    function SuggestionsModel () {
        this._suggestions = [];
        this._activeIndex = -1;
    }

    SuggestionsModel.prototype = {
        onChange: function () {},

        onHighlightChange: function () {},

        get: function (index) {
            return this._suggestions[index];
        },

        set: function (suggestions) {
            var oldSuggestions = this._suggestions;
            this._suggestions = suggestions || [];
            this.onChange(oldSuggestions);
            return this;
        },

        setHighlighted: function (toHighlight) {
            if (toHighlight) {
                for (var i = 0; i < this._suggestions.length; i++) {
                    if (this._suggestions[i].id === toHighlight.id) {
                        this.highlight(i);
                    }
                }
            }

            return this;
        },

        highlight: function (index) {
            this._activeIndex = index;
            this.onHighlightChange();
            return this;
        },

        highlightPrevious: function () {
            var current = this._activeIndex;
            var previousActiveIndex = (current === 0) ? current : (current - 1);
            this.highlight(previousActiveIndex);
            return this;
        },

        highlightNext: function () {
            var current = this._activeIndex;
            var nextActiveIndex = (current === this._suggestions.length - 1) ? current : (current + 1);
            this.highlight(nextActiveIndex);
            return this;
        },

        highlighted: function () {
            return this.get(this._activeIndex);
        },

        highlightedIndex: function () {
            return this._activeIndex;
        }
    };

    return SuggestionsModel;
});
