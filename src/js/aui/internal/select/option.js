define(['aui/internal/skate'], function (skate) {
    'use strict';

    skate('aui-option', {
        created: function(element) {
            Object.defineProperty(element, 'value', {
                get: function () {
                    return element.getAttribute('value') || element.textContent;
                },
                set: function (value) {
                    element.setAttribute('value', value);
                }
            });
        },
        prototype: {
            serialize: function () {
                var json = {};
                if (this.hasAttribute('img-src')) {
                    json['img-src'] = this.getAttribute('img-src');
                }
                json.value = this.value;
                json.label = this.textContent;

                return json;
            }
        }
    });
});
