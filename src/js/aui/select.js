define([
    'aui/internal/skate',
    'backbone',
    './internal/select/option',
    './internal/select/suggestions-model',
    './internal/select/suggestions-view',
    './internal/select/template',
    './internal/spinner',
    '../experimental-autocomplete/progressive-data-set'
], function (
    skate,
    Backbone,
    Option,
    SuggestionsModel,
    SuggestionsView,
    template,
    Spinner
) {
    'use strict';
    var $ = jQuery;
    var ProgressiveDataSet = AJS.ProgressiveDataSet;

    var SuggestionModel = Backbone.Model.extend({
            idAttribute: 'label'
        });

    function waitForAssistive (callback) {
        setTimeout(callback, 50);
    }

    function setBusyState (element) {
        element._spinner.show();
        element._button.setAttribute('aria-busy', 'true');
        element._input.setAttribute('aria-busy', 'true');
        element._dropdown.setAttribute('aria-busy', 'true');
    }

    function setIdleState (element) {
        element._spinner.hide();
        element._button.setAttribute('aria-busy', 'false');
        element._input.setAttribute('aria-busy', 'false');
        element._dropdown.setAttribute('aria-busy', 'false');
    }

    function matchPrefix (model, query) {
        var value = model.get('label').toLowerCase();
        return value.indexOf(query.toLowerCase()) === 0;
    }

    function setInitialState (element) {
        element._input.value = element.value;
        var imageSource = (element._suggestionModel.highlighted() && element._suggestionModel.highlighted().get('img-src'));
        if (imageSource) {
            setElementImage(element, imageSource);
        }
        element._suggestionsView.hide();
        element._input.setAttribute('aria-expanded', 'false');
    }

    function setElementImage (element, imageSource) {
        $(element._input).addClass('aui-select-has-inline-image');
        element._input.setAttribute('style', 'background-image: url(' + imageSource + ')');
    }

    function suggest (element, autoHighlight) {
        element._autoHighlight = autoHighlight;
        var query = element._input.value === element.value ? '' : element._input.value;

        element._suggestionModel.set(); // Clear list before new query.
        element._progressiveDataSet.query(query);
    }

    function selectHighlightedSuggestion (element) {
        setValueFromModel(element, element._suggestionModel.highlighted());
        setInitialState(element);
    }

    function setValueFromModel (element, model) {
        if (!model) {
            return;
        }

        var option = document.createElement('option');
        var select = element._select;

        option.setAttribute('selected', true);
        option.setAttribute('value', model.get('value') || model.get('label'));
        option.textContent = model.get('label');

        select.innerHTML = '';
        select.options.add(option);
        select.dispatchEvent(new CustomEvent('change', { bubbles: true }));
    }

    function convertOptionToModel (option) {
        return new SuggestionModel(option.serialize());
    }

    function convertOptionsToModels (element) {
        var models = [];

        for (var i = 0; i < element._datalist.children.length; i++) {
            var option = element._datalist.children[i];
            models.push(convertOptionToModel(option));
        }

        return models;
    }

    function initialiseProgressiveDataSet (element) {
        element._progressiveDataSet = new ProgressiveDataSet(convertOptionsToModels(element), {
            model: SuggestionModel,
            matcher: matchPrefix,
            queryEndpoint: element._queryEndpoint
        });

        // Progressive data set should indicate whether or not it is busy when processing any async requests.
        // Check if there's any active queries left, if so: set spinner and state to busy, else set to idle and remove
        // the spinner.
        element._progressiveDataSet.on('activity', function () {
            if (element._progressiveDataSet.activeQueryCount) {
                setBusyState(element);
            } else {
                setIdleState(element);
            }
        });

        // Progressive data set doesn't do anything if the query is empty so we
        // must manually convert all data list options into models.
        //
        // Otherwise progressive data set can do everything else for us:
        // 1. Sync matching
        // 2. Async fetching and matching
        element._progressiveDataSet.on('respond', function (data) {
            var optionToHighlight;

            // This means that a query was made before the input was cleared and
            // we should cancel the response.
            if (data.query && !element._input.value) {
                return;
            }

            if (!data.query) {
                data.results = convertOptionsToModels(element);
            }

            // Create a list of all IDs for the results
            var resultsIds = $.map(data.results, function (result) {
                return result.id;
            });

            var indexOfValueInResults = resultsIds.indexOf(element.value);
            if (indexOfValueInResults >= 0) {
                data.results.splice(indexOfValueInResults, 1);
            }

            element._suggestionModel.set(data.results);
            if (element._autoHighlight) {
                optionToHighlight = element._suggestionModel.highlighted() || data.results[0];
                element._suggestionModel.setHighlighted(optionToHighlight);
                waitForAssistive(function () {
                    element._input.setAttribute('aria-activedescendant', $(element._suggestionsView.el).find('.aui-select-active').attr('id'));
                });
            }
            element._input.setAttribute('aria-expanded', 'true');

            // If the response is async (append operation), has elements to append and has a highlighted element, we need to update the status.
            if (!element._isSync && element._suggestionsView.getActive()) {
                element.querySelector('.aui-select-status').innerHTML = AJS.I18n.getText('aui.select.new.suggestions');
            }

            element._suggestionsView.show();

            if (element._autoHighlight) {
                waitForAssistive(function () {
                    element._input.setAttribute('aria-activedescendant', $(element._suggestionsView.el).find('.aui-select-active').attr('id'));
                });
            }
        });
    }

    function associateDropdownAndTrigger (element) {
        element._dropdown.id = element._listId;
        element.querySelector('button').setAttribute('aria-controls', element._listId);
    }

    function bindHighlightMouseover (element) {
        $(element._dropdown).on('mouseover', 'li', function (e) {
            element._suggestionModel.highlight($(e.target).index());
        });
    }

    function bindSelectMousedown (element) {
        $(element._dropdown).on('mousedown', 'li', function (e) {
            element._suggestionModel.highlight($(e.target).index());
            selectHighlightedSuggestion(element);
            element._suggestionsView.hide();
            element._input.removeAttribute('aria-activedescendant');
        });
    }

    function bindInputBlur (element) {
        // Delegate blur event, skated blur events do not by default capture, which we need.
        element._input.addEventListener('blur', function () {
            if (element._suggestionsView.isVisible()) {
                element._suggestionModel._suggestions.forEach(function (suggestion) {
                    if (suggestion.id.toLowerCase() === element._input.value.toLowerCase()) {
                        setValueFromModel(element, suggestion);
                    }
                });
            }
            setInitialState(element);
        }, true);
    }

    function initialiseValue (element) {
        var option = element._datalist.querySelector('aui-option[selected]');

        if (option) {
            setValueFromModel(element, convertOptionToModel(option));
        }
    }

    return skate('aui-select', {
        type: skate.types.TAG,
        template: template,
        created: function (element) {
            element._listId = AJS.id();
            element._input = element.querySelector('input');
            element._select = element.querySelector('select');
            element._dropdown = element.querySelector('.aui-popover');
            element._datalist = element.querySelector('datalist');
            element._button = element.querySelector('button');
            element._suggestionsView = new SuggestionsView(element._dropdown, element._input);
            element._suggestionModel = new SuggestionsModel();

            element._suggestionModel.onChange = function (oldSuggestions) {
                var suggestionsToAdd = [];

                element._suggestionModel._suggestions.forEach(function (newSuggestion) {
                    var inArray = oldSuggestions.some(function (oldSuggestion) {
                        return newSuggestion.id === oldSuggestion.id;
                    });

                    if (!inArray) {
                        suggestionsToAdd.push(newSuggestion);
                    }
                });

                element._suggestionsView.render(suggestionsToAdd, oldSuggestions.length, element._listId);
            };

            element._suggestionModel.onHighlightChange = function () {
                var active = element._suggestionModel.highlightedIndex();
                element._suggestionsView.setActive(active);
                element._input.setAttribute('aria-activedescendant', $(element._suggestionsView.el).find('.aui-select-active').attr('id'));
            };

            Object.defineProperty(element, 'value', {
                get: function () {
                    var selected = this._select.options[this._select.selectedIndex];
                    return selected ? selected.textContent : '';
                },
                set: function (value) {
                    var data = this._progressiveDataSet;
                    var model = data.findWhere({
                        value: value
                    }) || data.findWhere({
                        label: value
                    });
                    setValueFromModel(this, model);
                    return this;
                }
            });

            skate.init(element); //make sure the children have been initialised too

            initialiseProgressiveDataSet(element);
            associateDropdownAndTrigger(element);
            element._input.setAttribute('aria-controls', element._listId);
            bindHighlightMouseover(element);
            bindSelectMousedown(element);
            bindInputBlur(element);
            initialiseValue(element);
            setInitialState(element);
            element._spinner = new Spinner(element._button);
            setIdleState(element);
        },
        attributes: {
            'id': function (element, data) {
                if (element.id) {
                    element.querySelector('input').id = data.newValue;
                    element.removeAttribute('id');
                }
            },
            'name': function (element, data) {
                element.querySelector('select').setAttribute('name', data.newValue);
            },
            'src': function (element, data) {
                element._queryEndpoint = data.newValue;
                element._isSync = element._queryEndpoint ? false : true;
            }
        },
        events: {
            'click button': function (element) {
                suggest(element, false);
                element._input.focus();
            },
            'input' : function (element) {
                suggest(element, true);
            },
            'keydown input': function (element, e) {
                var isVisible = element._suggestionsView.isVisible();
                var currentValue = element._input.value;
                var handled = false;

                if (e.keyCode === AJS.keyCode.ESCAPE) {
                    setInitialState(element);
                    element._input.setAttribute('aria-expanded', 'false');
                    return;
                }

                if (isVisible) {
                    if (e.keyCode === AJS.keyCode.ENTER) {
                        selectHighlightedSuggestion(element);
                        e.preventDefault();
                    }
                    else if (e.keyCode === AJS.keyCode.TAB) {
                        selectHighlightedSuggestion(element);
                        handled = true;
                        element._input.setAttribute('aria-expanded', 'false');
                    }
                    else if (e.keyCode === AJS.keyCode.UP) {
                        element._suggestionModel.highlightPrevious();
                        e.preventDefault();
                    }
                    else if (e.keyCode === AJS.keyCode.DOWN) {
                        element._suggestionModel.highlightNext();
                        e.preventDefault();
                    }
                }
                else {
                    if (e.keyCode === AJS.keyCode.UP || e.keyCode === AJS.keyCode.DOWN) {
                        suggest(element, true);
                        e.preventDefault();
                    }
                }

                handled = handled || e.defaultPrevented;

                setTimeout(function emulateCrossBrowserInputEvent () {
                    if (element._input.value !== currentValue && !handled) {
                        element.dispatchEvent(new CustomEvent('input', { bubbles: true }));
                    }
                }, 0);
            }
        }
    });
});
