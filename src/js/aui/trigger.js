;(function(init) {
    'use strict';

    var ret = init(AJS.$, window.skate);
    define(function() {
        return ret;
    });
})(function ($, skate) {
    'use strict';


    function findControlled(trigger) {
        return document.getElementById(trigger.getAttribute('aria-controls'));
    }

    function triggerMessage(trigger, e) {
        if (trigger.isEnabled()) {
            var component = findControlled(trigger);
            if (component && component.message) {
                component.message(e);
            }
        }
    }

    skate('data-aui-trigger', {
        type: skate.types.ATTR,
        attached: function(trigger) {
            $(trigger).on({
                click: function(e) {
                    triggerMessage(trigger, e);
                    e.preventDefault();
                },
                'mouseenter mouseleave focus blur': function(e) {
                    triggerMessage(trigger, e);
                }
            });
        },
        prototype: {
            disable: function() {
                this.setAttribute('aria-disabled', 'true');
            },
            enable: function() {
                this.setAttribute('aria-disabled', 'false');
            },
            isEnabled: function() {
                return this.getAttribute('aria-disabled') !== 'true';
            }
        }
    });
});
