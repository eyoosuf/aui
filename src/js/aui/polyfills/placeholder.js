;(function(init) {
    'use strict';

    define(function () {
        return init(AJS.$, window.skate);
    });
})(function ($, skate) {
    'use strict';

    if('placeholder' in document.createElement('input')) {
        return;
    }

    skate('placeholder', {
        type: skate.types.ATTR,
        attached: function(el) {
            giveInputPlaceholderPolyfill(el);
        }
    });

    function giveInputPlaceholderPolyfill(input) {
        var $input = $(input);
        applyDefaultText($input);

        $input.blur(function() {
            applyDefaultText($input);
        });

        $input.focus(function() {
            if($input.hasClass("aui-placeholder-shown")) {
                $input.val("");
                $input.removeClass("aui-placeholder-shown");
            }
        });
    }

    function applyDefaultText($input) {
        if(!$.trim($input.val()).length) {
            $input.val($input.attr("placeholder"));
            $input.addClass("aui-placeholder-shown");
        }
    }


});