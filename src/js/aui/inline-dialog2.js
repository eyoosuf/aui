define([
    'aui/internal/skate',
    './internal/alignment',
    './trigger',
    '../layer'
], function () {
    'use strict';

    var DEFAULT_HOVEROUT_DELAY = 1000;

    var $ = window.jQuery;
    var Alignment = AJS.Alignment;
    var Layer = AJS.layer;
    var skate = window.skate;

    function getTrigger (element) {
        return document.querySelector('[aria-controls="' + element.id + '"]');
    }

    function doIfTrigger (element, callback) {
        var trigger = getTrigger(element);

        if (trigger) {
            callback(trigger);
        }
    }

    function doIfHover (element, callback) {
        var isHover = element.getAttribute('responds-to') === 'hover';

        if (isHover) {
            callback(element);
        }
    }

    function initAlignment (element, trigger) {
        if (!element._auiAlignment) {
            element._auiAlignment = new Alignment(element, trigger);
        }
    }

    function enableAlignment (element, trigger) {
        initAlignment(element, trigger);
        element._auiAlignment.enable();
    }

    function disableAlignment (element, trigger) {
        initAlignment(element, trigger);
        element._auiAlignment.disable();
    }

    function handleMessage (element, message) {
        var messageTypeMap = {
            toggle: ['click'],
            hover: ['mouseenter', 'mouseleave', 'focus', 'blur']
        };
        var respondsTo = element.getAttribute('responds-to') || element.getAttribute('data-aui-responds-to');
        var messageList = messageTypeMap[respondsTo];

        if (messageList && messageList.indexOf(message.type) > -1) {
            messageHandler[message.type](element, message);
        }
    }

    var messageHandler = {
        click: function (element) {
            if (element.isVisible()) {
                if (!Layer(element).isPersistent()) {
                    element.hide();
                }
            }
            else {
                element.show();
            }
        },

        mouseenter: function (element) {
            if (!element.isVisible()) {
                element.show();
            }

            if (element._clearMouseleaveTimeout) {
                element._clearMouseleaveTimeout();
            }
        },

        mouseleave: function (element) {
            if (Layer(element).isPersistent() || !element.isVisible()) {
                return;
            }

            if (element._clearMouseleaveTimeout) {
                element._clearMouseleaveTimeout();
            }

            var timeout = setTimeout(function () {
                if (!element.mouseInside) {
                    element.hide();
                }
            }, DEFAULT_HOVEROUT_DELAY);

            element._clearMouseleaveTimeout = function () {
                clearTimeout(timeout);
                element._clearMouseleaveTimeout = null;
            };
        },

        focus: function (element) {
            if (!element.isVisible()) {
                element.show();
            }
        },

        blur: function (element) {
            if (!Layer(element).isPersistent() && element.isVisible()) {
                element.hide();
            }
        }
    };

    return skate('aui-inline-dialog2', {
        type: skate.types.TAG,

        prototype: {
            /**
             * Shows the inline dialog.
             *
             * @returns {HTMLElement}
             */
            show: function () {
                var that = this;

                Layer(this).show();

                doIfTrigger(this, function (trigger) {
                    enableAlignment(that, trigger);
                    trigger.setAttribute('aria-expanded', 'true');
                });

                return this;
            },

            /**
             * Hides the inline dialog.
             *
             * @returns {HTMLElement}
             */
            hide: function () {
                var that = this;

                Layer(this).hide();

                doIfTrigger(this, function (trigger) {
                    disableAlignment(that, trigger);
                    trigger.setAttribute('aria-expanded', 'false');
                });

                return this;
            },

            /**
             * Returns whether or not the inline dialog is visible.
             *
             * @returns {HTMLElement}
             */
            isVisible: function () {
                return Layer(this).isVisible();
            },

            /**
             * Handles the receiving of a message from another component.
             *
             * @param {Object} message The message to act on.
             *
             * @returns {HTMLElement}
             */
            message: function (message) {
                handleMessage(this, message);
                return this;
            }
        },

        created: function (element) {
            doIfTrigger(element, function (trigger) {
                trigger.setAttribute('aria-expanded', element.isVisible());
                trigger.setAttribute('aria-haspopup', 'true');
            });

            doIfHover(element, function () {
                element.mouseInside = false;

                element.addEventListener('mouseenter', function () {
                    element.mouseInside = true;
                    element.message({
                        type: 'mouseenter'
                    });
                });

                element.addEventListener('mouseleave', function () {
                    element.mouseInside = false;
                    element.message({
                        type: 'mouseleave'
                    });
                });
            });
        },

        detached: function (element) {
            if (element._auiAlignment) {
                element._auiAlignment.destroy();
            }
        },

        template: function (element) {
            $(element)
                .addClass('aui-layer')
                .attr('aria-hidden', 'true')
                .html('<div class="aui-inline-dialog-contents">' + element.innerHTML + '</div>');
        },
    });
});
