define(['aui/internal/spinner', 'aui/internal/state'], function (Spinner, State) {
    'use strict';

    /*
     Can't depend on jQuery or skate since they won't be defined in product environments (thanks to deletion of the define.amd object)
     */
    var $ = AJS.$;
    var skate = window.skate;

    function setSpinner (button) {
        State(button).set('spinner', new Spinner(button));
    }

    function getSpinner (button) {
        return State(button).get('spinner');
    }

    function isBusy(button) {
        var ariaBusy = button.getAttribute('aria-busy');
        return ariaBusy && ariaBusy === 'true';
    }

    function isInputNode(button) {
        return button.nodeName === 'INPUT';
    }

    skate('aui-button', {
        type: skate.types.CLASS,

        created: function(element) {
            if (!element.hasAttribute('aria-busy')) {
                element.setAttribute('aria-busy', false);
            }
        },

        prototype: {
            /**
             * Adds a spinner to the button and hides the text
             *
             * @returns {HTMLElement}
             */
            busy: function() {
                if (isInputNode(this) || isBusy(this)) {
                    AJS.warn('It is not valid to call busy() on an input button.');
                    return this;
                }

                setSpinner(this);
                this.setAttribute('aria-busy', true);

                return this;
            },

            /**
             * Removes the spinner and shows the tick on the button
             *
             * @returns {HTMLElement}
             */
            idle: function() {
                if (isInputNode(this) || !isBusy(this)) {
                    AJS.warn('It is not valid to call idle() on an input button.');
                    return this;
                }

                getSpinner(this).remove();
                this.setAttribute('aria-busy', false);

                return this;
            },

            /**
             * Removes the spinner and shows the tick on the button
             *
             * @returns {Boolean}
             */
            isBusy: function() {
                if (isInputNode(this)) {
                    AJS.warn('It is not valid to call isBusy() on an input button.');
                    return false;
                }

                return isBusy(this);
            }
        }
    });
});
