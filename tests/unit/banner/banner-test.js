/* jshint expr:true */
define([
    '../../helpers/all.js',
    'aui/banner',
    'jquery'
], function(
    helpers,
    banner,
    $
) {
    'use strict';

    describe('Banner', function () {
        it('cannot be created unless a #header is present', function() {
            try {
                expect(banner({body: 'test'})).to.throw(Error);
            } catch (e) {
                expect(e.message && e.message.indexOf('header')).to.be.above(-1);
            }
        });

        describe('Banners', function () {
            var header;
            beforeEach(function() {
                header = $('<div id="header"></div>');
                header.html('<p>test</p><nav class="aui-header"></nav>');
                $('#test-fixture').append(header);
            });

            it('should return a DOM element', function() {
                var b = banner({ body: 'test banner' });
                expect(b).to.be.an.instanceof(HTMLElement);
            });

            it('are prepended to the header', function() {
                var b = banner({ body: 'test banner' });
                expect(header.children().length).to.equal(3);
                expect(header.children().get(0)).to.equal(b);
            });

            it('can have a body that accepts html', function() {
                var b = banner({ body: 'with an <strong>important <a href="#">hyperlink</a></strong>!' });
                expect(b.textContent, 'with an important hyperlink!');
                expect(b.querySelectorAll('strong').length).to.equal(1);
                expect(b.querySelectorAll('a').length).to.equal(1);
            });

            it('can have a title that accepts text', function () {
                var b = banner({ title: 'my <title>' });
                expect(b.querySelector('.aui-banner-title').innerHTML).to.equal('my &lt;title&gt;');
            })
        });
    });
});
