/* jshint expr:true */
define([
    '../../helpers/all.js',
    'aui/flag',
    'jquery'
], function (
    helpers,
    flag,
    $
) {
    'use strict';

    function isVisible (element) {
        return $(element).attr('aria-hidden') !== 'true';
    }

    describe('Flags -', function () {
        beforeEach(function () {
            $('#test-fixture').append('<div id="aui-flag-container"></div>');
        });

        afterEach(function () {
            $('#aui-flag-container').remove();
        });

        function setupInfoFlag () {
            var title = 'Title';
            var body = 'This message should float over the screen';

            flag({
                type: 'info',
                title: title,
                body: body,
                persistent: false
            });

            return {
                title: title,
                body: body
            };
        }

        function setupDefaultFlag () {
            flag({body: 'This is a message with nearly all options default'});
        }

        function getFlagText () {
            return $('.aui-flag');
        }

        function closeFlag () {
            $('.aui-flag .icon-close').click();
        }

        it('Floating message is present on the screen', function () {
            setupInfoFlag();
            expect(getFlagText().length).to.equal(1);
            expect(isVisible(getFlagText())).to.equal(true);
        });

        it('Floating messages HTML contain the title and contents somewhere', function () {
            var flagProperties = setupInfoFlag();
            expect(getFlagText().html().indexOf(flagProperties.title)).to.not.equal(-1);
            expect(getFlagText().html().indexOf(flagProperties.body)).to.not.equal(-1);
        });

        it('Messages appear with mostly default options', function () {
            setupDefaultFlag();
            expect(getFlagText().length).to.equal(1);
        });

        it('Closing message triggers a close event on the message', function() {
            var flagTrigger;
            var flagElement = $(flag({body: 'Close me'})).on('aui-flag-close', function () {
                flagTrigger = this;
            })[0];

            closeFlag();
            expect(flagTrigger).to.equal(flagElement);
        });

        it('Flags can be closed via the API method', function () {
            setupInfoFlag();
            expect(getFlagText().attr('aria-hidden')).to.equal('false');
            getFlagText()[0].close();
            expect(getFlagText().attr('aria-hidden')).to.equal('true');
        });

        describe('When a flag is constructed via the JS API', function () {
            it('should return the raw DOM element', function () {
                expect(flag({})).to.be.an.instanceof(Element);
            });
        });

        describe('When a flag is shown', function () {
            var clock;
            var flagElement;

            beforeEach(function () {
                clock = sinon.useFakeTimers();
            });

            afterEach(function () {
                clock.restore();
            });


            function isVisible (element) {
                return $(element).attr('aria-hidden') !== 'true';
            }

            function hasCloseIcon(element) {
                return !!$(element).find('.icon-close').length;
            }

            beforeEach(function () {
                clock = sinon.useFakeTimers();
            });

            afterEach(function () {
                clock.restore();
            });

            describe('with the title option', function () {
                function hasNoTitle(flagElement) {
                    return $(flagElement).find('p.title').text() === '';
                }
                it('set to null, it has no title', function () {
                    flagElement = flag({title: null});
                    expect(hasNoTitle(flagElement)).to.be.true;
                });

                it('set to false, it has no title', function () {
                    flagElement = flag({title: false});
                    expect(hasNoTitle(flagElement)).to.be.true;
                });

                it('unset, it has no title', function () {
                    flagElement = flag({});
                    expect(hasNoTitle(flagElement)).to.be.true;
                });
            });

            describe('with the body option', function () {
                function hasNoBody(flagElement) {
                    return $(flagElement).text() === '';
                }
                it('set to null, it has no body', function () {
                    flagElement = flag({body: null});
                    expect(hasNoBody(flagElement)).to.be.true;
                });

                it('set to false, it has no body', function () {
                    flagElement = flag({body: false});
                    expect(hasNoBody(flagElement)).to.be.true;
                });

                it('unset, it has no body', function () {
                    flagElement = flag({});
                    expect(hasNoBody(flagElement)).to.be.true;
                });
            });

            describe('with the close option', function () {
                describe('set to "manual"', function () {
                    beforeEach(function () {
                        flagElement = flag({close: 'manual'});
                    });

                    it('the flag should not close after five seconds', function () {
                        clock.tick(10000);
                        expect(isVisible(flagElement)).to.equal(true);
                    });

                    it('the flag should have a close icon', function () {
                        expect(hasCloseIcon(flagElement)).to.equal(true);
                    });
                });

                describe('set to "auto"', function () {
                    beforeEach(function () {
                        flagElement = flag({close: 'auto'});
                    });

                    it('the flag should close after five seconds', function () {
                        clock.tick(10000);
                        expect(isVisible(flagElement)).to.equal(false);
                    });

                    it('the flag should have a close icon', function () {
                        expect(hasCloseIcon(flagElement)).to.equal(true);
                    });
                });

                describe('set to "never"', function () {
                    beforeEach(function () {
                        flagElement = flag({close: 'never'});
                    });

                    it('the flag should not close after five seconds', function () {
                        clock.tick(10000);
                        expect(isVisible(flagElement)).to.equal(true);
                    });

                    it('the flag should not have a close icon', function () {
                        expect(hasCloseIcon(flagElement)).to.equal(false);
                    });
                });

                describe('left as default', function () {
                    beforeEach(function () {
                        flagElement = flag({});
                    });

                    it('the flag should not close after five seconds', function () {
                        clock.tick(10000);
                        expect(isVisible(flagElement)).to.equal(true);
                    });

                    it('the flag should have a close icon', function () {
                        expect(hasCloseIcon(flagElement)).to.equal(true);
                    });
                });
            });
        });
    });
});
