/*jshint expr:true */
define([
    'aui/select',
    'aui/internal/skate',
    'jquery',
    '../../helpers/all.js',
    'layer',
    'layer-manager-global',
    'aui',
    'keyCode'
], function (
    SingleSelect,
    skate,
    $,
    helpers
) {
    'use strict';

    var Backbone = window.Backbone;
    var clock;

    function createAndSkate (html) {
        var fixtures = helpers.fixtures({
            singleSelect: html,
            styles: '<style>[aria-hidden=true] { display: none !important; } .active { background-color: yellow; } </style>'
        });

        return skate.init(fixtures.singleSelect);
    }

    function getInputFor (singleSelect) {
        return singleSelect._input;
    }

    function getDropdownFor (singleSelect) {
        return singleSelect._dropdown;
    }

    function getTriggerFor (singleSelect) {
        return singleSelect.querySelector('button');
    }

    function getHighlightedDropdownItem (dropdown) {
        return dropdown.querySelector('.aui-select-active');
    }

    function getResultsAsArray (singleSelect) {
        var anchors = singleSelect._dropdown.querySelectorAll('li');
        var options = [];

        for (var a = 0; a < anchors.length; a++) {
            options.push(anchors[a].textContent);
        }

        return options;
    }

    describe('Single select', function () {
        var singleSelect;
        var dropdown;

        beforeEach(function () {
            singleSelect = undefined;
            dropdown = undefined;
        });

        it('can be initialised', function () {
            singleSelect = new SingleSelect();
            expect(singleSelect.querySelector('select')).to.exist;
        });

        it('is constructed with an associated dropdown', function () {
            singleSelect = new SingleSelect();
            dropdown = getDropdownFor(singleSelect);
            expect(dropdown).to.be.an.instanceof(HTMLElement);
        });

        describe('should expose the value', function () {
            it('for getting', function () {
                singleSelect = new SingleSelect();
                expect(singleSelect.value).to.be.empty;
            });

            it('for setting', function () {
                singleSelect = createAndSkate('<aui-select><aui-option>Option 1</aui-option><aui-option>Option 2</aui-option><aui-option>Option 3</aui-option></aui-select>');
                singleSelect.value = 'Option 2';
                expect(singleSelect.value).to.equal('Option 2');
            });
        });

        describe('should propagate the name attribute to the nested <select> form element', function () {
            beforeEach(function () {
                singleSelect = createAndSkate('<aui-select name="foo"></aui-select>');
            });

            it('when it is initialised', function () {
                singleSelect.querySelector('select').name.should.equal('foo');
            });

            it('when it changes', function (done) {
                singleSelect.setAttribute('name', 'bar');
                helpers.afterMutations(function () {
                    singleSelect.querySelector('select').name.should.equal('bar');
                    done();
                });
            });
        });

        describe('should move the id attribute to the nested <input> form element', function () {
            // Because otherwise, we fail at accessibility if there is a <label> for the field.
            // Until we define an <aui-field> element, we'll have to deal with this here.

            beforeEach(function () {
                singleSelect = createAndSkate('<aui-select id="foo"></aui-select>');
            });

            it('when it is initialised', function () {
                expect(getInputFor(singleSelect).id).to.equal('foo');
                expect(singleSelect.id).to.be.empty;
            });

            it('when it changes (or is re-added)', function (done) {
                singleSelect.setAttribute('id', 'bar');
                helpers.afterMutations(function () {
                    expect(getInputFor(singleSelect).id).to.equal('bar');
                    expect(singleSelect.id).to.be.empty;
                    done();
                });
            });
        });

        describe('when no <aui-options> are provided', function () {
            it('starts with no selected value', function () {
                singleSelect = createAndSkate('<aui-select></aui-select>');
                expect(singleSelect.value).to.be.empty;
            });
        });

        describe('when <aui-options> are provided', function () {
            it('should store user defined <aui-option>s', function () {
                singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option>Two</aui-option><aui-option>Three</aui-option></aui-select>');
                singleSelect.querySelectorAll('aui-option').length.should.equal(3);
            });

            it('starts with no selected value if none of the <aui-options> have the "selected" attribute', function () {
                singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option>Two</aui-option><aui-option>Three</aui-option></aui-select>');
                expect(singleSelect.value).to.be.empty;
            });

            it('starts with selected value of first <aui-option> with "selected" attribute', function () {
                // This is congruous with what the w3c spec says about the <select> element: http://www.w3.org/TR/html5/forms.html#dom-select-selectedindex
                singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option selected>Two</aui-option><aui-option selected>Three</aui-option></aui-select>');
                expect(singleSelect.value).to.equal('Two');
            });
        });
    });

    describe('Single select', function () {
        var singleSelect;
        var dropdown;
        var input;

        beforeEach(function () {
            singleSelect = createAndSkate('<aui-select name="foo"><aui-option>Option 1</aui-option><aui-option>Option 2</aui-option><aui-option value="third-option">Third Option</aui-option></aui-select>');
            dropdown = getDropdownFor(singleSelect);
            input = getInputFor(singleSelect);
            clock = sinon.useFakeTimers();
        });

        afterEach(function() {
            clock.restore();
            helpers.removeLayers();
        });

        it('exposes an empty selected value', function () {
            expect(singleSelect.value).to.be.empty;
        });

        describe('when no valid suggestions can be attained', function () {
            // control misconfiguration/error state.
        });

        describe('when valid suggestions can be attained', function () {
            describe('and the <input> is focused', function () {
                beforeEach(function () {
                    input.focus();
                });

                afterEach(function () {
                    expect(document.activeElement).to.equal(input);
                });

                it('should not open the dropdown', function () {
                    expect(dropdown).to.not.be.visible;
                });

                it('shows the dropdown when the up key is pressed', function () {
                    helpers.pressKey(AJS.keyCode.UP);
                    expect(dropdown).to.be.visible;
                    helpers.pressKey(AJS.keyCode.UP);
                    expect(dropdown).to.be.visible;
                });

                it('shows the dropdown when the down key is pressed', function () {
                    helpers.pressKey(AJS.keyCode.DOWN);
                    expect(dropdown).to.be.visible;
                    helpers.pressKey(AJS.keyCode.DOWN);
                    expect(dropdown).to.be.visible;
                });

                it('shows the dropdown when a character is typed', function () {
                    helpers.fakeTypingOut('a');
                    expect(dropdown).to.be.visible;
                    helpers.fakeTypingOut('b');
                    expect(dropdown).to.be.visible;
                });

                it('shows the dropdown when the trigger is clicked', function () {
                    var trigger = getTriggerFor(singleSelect);
                    helpers.click(trigger);
                    expect(dropdown).to.be.visible;
                });
            });
        });

        describe('when the dropdown is shown', function () {
            beforeEach(function () {
                input.focus();
                helpers.pressKey(AJS.keyCode.UP);
            });

            it('should be visible', function () {
                expect(dropdown).to.be.visible;
            });

            it('pressing escape should close it', function () {
                helpers.pressKey(AJS.keyCode.ESCAPE);
                expect(dropdown).to.not.be.visible;
            });
        });

        describe('when the dropdown is shown via the keyboard after click focus', function () {
            var fakeOption;
            var highlightedStub;

            beforeEach(function () {
                fakeOption = new (Backbone.Model.extend({ idAttribute: 'label' }))();
                fakeOption.set('label', 'fake option');
                fakeOption.set('value', 'fake option');
                highlightedStub = sinon.stub(singleSelect._suggestionModel, 'highlighted').returns(fakeOption);

                helpers.click(input);
                input.focus();
                helpers.pressKey(AJS.keyCode.DOWN);
                expect(dropdown).to.be.visible;
            });

            afterEach(function () {
                highlightedStub.restore();
            });

            it('should close the dropdown when the value is set', function () {
                helpers.pressKey(AJS.keyCode.TAB);
                expect(dropdown).to.not.be.visible;

                // Might've accidentally appeared again as a result of user input!
                clock.tick(1);
                expect(dropdown).to.not.be.visible;
            });
        });

        describe('when the dropdown is shown via the keyboard', function () {
            beforeEach(function () {
                input.focus();
                helpers.pressKey(AJS.keyCode.DOWN);
                expect(dropdown).to.be.visible;
            });

            it('highlights the first suggestion', function () {
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
            });

            it('closes the dropdown when pressing escape', function () {
                helpers.pressKey(AJS.keyCode.ESCAPE);
                expect(dropdown).to.not.be.visible;
            });

            it('reverts value to original when escape is pressed', function () {
                var originalValue = singleSelect.value;
                helpers.fakeTypingOut('and more');
                helpers.pressKey(AJS.keyCode.ESCAPE);
                expect(input.value).to.equal(originalValue);
            });

            describe('and the middle suggestion is highlighted in the dropdown', function () {
                beforeEach(function () {
                    singleSelect._suggestionModel.highlight(1); // Kinda cheating, kinda not.
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 2');
                });

                it('selects next item in dropdown when down arrow is pressed', function () {
                    helpers.pressKey(AJS.keyCode.DOWN);
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
                });

                it('selects previous item in dropdown when up arrow is pressed', function () {
                    helpers.pressKey(AJS.keyCode.UP);
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
                });

                it('should highlight the best suggestion when autohighlighting is on', function () {
                    input.focus();
                    helpers.fakeTypingOut('o');
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 2');
                    helpers.fakeBackspace();
                    helpers.fakeTypingOut('t');
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
                    helpers.fakeBackspace();
                    helpers.fakeTypingOut('o');
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
                });
            });

            describe('and the first suggestion is highlighted in the dropdown', function () {
                beforeEach(function () {
                    singleSelect._suggestionModel.highlight(0); // Kinda cheating, kinda not.
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
                });

                it('does not loop to last suggestion when the up arrow is pressed', function () {
                    helpers.pressKey(AJS.keyCode.UP);
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
                });
            });

            describe('and the last suggestion is highlighted in the dropdown', function () {
                beforeEach(function () {
                    singleSelect._suggestionModel.highlight(2); // Kinda cheating, kinda not.
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
                });

                it('does not loop to first suggestion when the down arrow is pressed', function () {
                    helpers.pressKey(AJS.keyCode.DOWN);
                    expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
                });
            });

            describe('and a valid suggestion is highlighted in the dropdown', function () {
                var fakeOption;
                var highlightedStub;

                beforeEach(function () {
                    fakeOption = new (Backbone.Model.extend({ idAttribute: 'label' }))();
                    fakeOption.set('label', 'fake option');
                    fakeOption.set('value', 'fake option');
                    highlightedStub = sinon.stub(singleSelect._suggestionModel, 'highlighted').returns(fakeOption);
                });

                afterEach(function () {
                    highlightedStub.restore();
                });

                it('should set the value when the user presses enter', function () {
                    helpers.pressKey(AJS.keyCode.ENTER);
                    expect(singleSelect.value).to.equal('fake option');
                });

                it('should set the value when the user presses tab', function () {
                    helpers.pressKey(AJS.keyCode.TAB);
                    expect(singleSelect.value).to.equal('fake option');
                });

                it('should update the text in the <input> when the value is set', function () {
                    helpers.pressKey(AJS.keyCode.ENTER);
                    expect(input.value).to.equal('fake option');
                });

                it('should close the dropdown when the value is set', function () {
                    helpers.pressKey(AJS.keyCode.TAB);
                    expect(dropdown).to.not.be.visible;

                    // Might've accidentally appeared again as a result of user input!
                    clock.tick(1);
                    expect(dropdown).to.not.be.visible;
                });
            });

            describe('and no valid suggestions are highlighted', function () {
                var originalValue;
                var highlightedStub;

                beforeEach(function () {
                    originalValue = singleSelect.value;
                    helpers.fakeTypingOut('invalid option');
                    highlightedStub = sinon.stub(singleSelect._suggestionModel, 'highlighted').returns(undefined);
                });

                afterEach(function () {
                    highlightedStub.restore();
                });

                it('reverts value to original when enter is pressed', function() {
                    helpers.pressKey(AJS.keyCode.ENTER);
                    expect(singleSelect.value).to.equal(originalValue);
                });

                it('reverts value to original when tab is pressed', function () {
                    helpers.pressKey(AJS.keyCode.TAB);
                    expect(singleSelect.value).to.equal(originalValue);
                });

                it('should close the dropdown when tab is pressed', function () {
                    helpers.pressKey(AJS.keyCode.TAB);
                    expect(dropdown).to.not.be.visible;

                    // Might've accidentally appeared again as a result of user input!
                    clock.tick(1);
                    expect(dropdown).to.not.be.visible;
                });
            });
        });

        describe('when the dropdown is shown via the mouse', function () {
            beforeEach(function () {
                helpers.click(getTriggerFor(singleSelect));
            });

            it('should not highlight the first suggestion', function () {
                expect(getHighlightedDropdownItem(dropdown)).to.be.null;
            });

            it('does not hide the dropdown when the user clicks the <input>', function () {
                var input = getInputFor(singleSelect);

                helpers.click(input);
                expect(dropdown).to.be.visible;
            });

            it('does not hide the dropdown when the user clicks the dropdown container', function () {
                var ddContainer = getDropdownFor(singleSelect);

                helpers.click(ddContainer);
                expect(dropdown).to.be.visible;
            });

            it('hides the dropdown when the user clicks outside the <input> and dropdown container', function () {
                helpers.click(document.body);
                expect(dropdown).to.not.be.visible;
            });

            it('reverts value to original when the clicks outside the <input> and dropdown container', function () {
                singleSelect.value = 'Option 2';
                helpers.fakeTypingOut('Opt 1');
                helpers.click(document.body);
                expect(singleSelect.value).to.equal('Option 2');
            });

            it('does not highlight any suggestions when the trigger is clicked', function () {
                expect(getHighlightedDropdownItem(dropdown)).to.not.be.defined;
            });

            it('should focus the input box when the trigger is clicked', function () {
                expect(getInputFor(singleSelect)).to.be.equal(document.activeElement);
            });

            describe('and the mouse is over a suggestion', function () {
                var currentOption;

                beforeEach(function () {
                    currentOption = getDropdownFor(singleSelect).querySelector('li:nth-child(2)');
                });

                it('should highlight that suggestion', function () {
                    currentOption.dispatchEvent(new CustomEvent('mouseover', { bubbles: true }));
                    expect(singleSelect._suggestionModel.highlightedIndex()).to.equal(1);
                });

                it('should not highlight a suggestion if the dropdown is closed and reopened', function () {
                    currentOption.dispatchEvent(new CustomEvent('mouseover', { bubbles: true }));
                    expect(singleSelect._suggestionModel.highlightedIndex()).to.equal(1);
                    helpers.click(document.body);
                    helpers.click(getTriggerFor(singleSelect));
                    expect(getHighlightedDropdownItem(dropdown)).to.not.be.defined;
                });

                describe('when the suggestion is clicked', function () {
                    it('should select that suggestion', function () {
                        var newOption = getDropdownFor(singleSelect).querySelector('li:nth-child(1)');
                        helpers.mousedown(newOption);
                        expect(singleSelect.value).to.equal(newOption.textContent);
                    });

                    it('should close the dropdown', function () {
                        helpers.mousedown(currentOption);
                        expect(dropdown).to.not.be.visible;
                    });
                });
            });
        });

        describe('with a selected value', function () {
            beforeEach(function () {
                input.focus();
                helpers.pressKey(AJS.keyCode.DOWN); // To open the dropdown; first will be highlighted
                helpers.pressKey(AJS.keyCode.DOWN); // Highlight the second option
                helpers.pressKey(AJS.keyCode.ENTER); // Select the highlighted option
            });

            it('should return the selected value', function () {
                expect(singleSelect.value).to.equal('Option 2');
            });

            it('should render text of initially selected value in the <input>', function () {
                expect(getInputFor(singleSelect).value).to.equal('Option 2');
            });

            describe('in a form', function () {
                var form;
                var $form;

                beforeEach(function () {
                    form = document.createElement('form');
                    $form = $(form);
                    form.setAttribute('id', 'test-form');
                    form.appendChild(singleSelect);
                    document.body.appendChild(form);
                    form.appendChild(singleSelect);
                });

                afterEach(function () {
                    $('#test-form').remove();
                });

                it('serialises the option label if no value is provided', function () {
                    var data = $(form).serialize();

                    data.should.be.a('string');
                    data.should.equal('foo=Option+2');
                });

                it('serialises the option value if one is provided', function () {
                    singleSelect.value = 'Third Option';
                    expect($form.serialize()).to.equal('foo=third-option');
                });
            });

            describe('and the select is opened again', function () {
                beforeEach(function () {
                    input.focus();
                    helpers.pressKey(AJS.keyCode.DOWN);
                });

                it('the selected value should not be shown in results', function () {
                    var results = getResultsAsArray(singleSelect);
                    expect(results).to.not.contain(singleSelect.value);
                });
            });
        });
    });


    describe('Single select matching', function () {
        var singleSelect;
        var input;

        function getResultsForTyping (text) {
            input.value = '';
            input.focus();
            helpers.fakeTypingOut(text);
            return getResultsAsArray(singleSelect);
        }

        beforeEach(function() {
            clock = sinon.useFakeTimers();
            singleSelect = createAndSkate([
                '<aui-select>',
                '<aui-option>zero</aui-option>',
                '<aui-option>one</aui-option>',
                '<aui-option>two</aui-option>',
                '<aui-option>three</aui-option>',
                '<aui-option>four</aui-option>',
                '<aui-option>five</aui-option>',
                '<aui-option>twenty</aui-option>',
                '<aui-option>twenty one</aui-option>',
                '<aui-option>one hundred</aui-option>',
                '</aui-select>'
            ].join(''));

            input = getInputFor(singleSelect);
        });

        afterEach(function() {
            clock.restore();
            helpers.removeLayers();
        });

        describe('with the default prefix matcher', function () {
            describe('user types "z"', function() {
                it('should see a list of one result containing the option "zero"', function() {
                    var results = getResultsForTyping('z');

                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['zero']);
                });
            });

            describe('user types "f"', function() {
                it('should see list of two results containing "four" and "five"', function() {
                    var results = getResultsForTyping('f');

                    expect(results.length).to.equal(2);
                    expect(results).to.include.members(['four', 'five']);
                });
            });

            describe('user types "four"', function() {
                it('should see a list of one result containing the option "four"', function() {
                    var results = getResultsForTyping('four');

                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['four']);
                });
            });

            describe('user types "one"', function() {
                var results;

                beforeEach(function() {
                    results = getResultsForTyping('one');
                });

                it('should see list of two results containing "one" and "one hundred"', function() {
                    expect(results.length).to.equal(2);
                    expect(results).to.include.members(['one', 'one hundred']);
                });

                it('should not see "twenty one"', function () {
                    expect(results).to.not.include.members(['twenty one']);
                });
            });

            describe('user types "one hundred"', function() {
                var results;

                beforeEach(function() {
                    results = getResultsForTyping('one hundred');
                });

                it('should see "one hundred" in the suggestions list', function() {
                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['one hundred']);
                });

                it('should not see "one"', function () {
                    expect(results).to.not.include.members(['one']);
                });

                describe('then clears', function () {
                    it('and blurs input, should see "one hundred" in input', function () {
                        helpers.pressKey(AJS.keyCode.ENTER);
                        helpers.fakeClear(input);
                        input.blur();
                        expect(input.value).to.equal('one hundred');
                    });

                    it('and types "one", then blurs input, should see "one" in input', function () {
                        helpers.pressKey(AJS.keyCode.ENTER);
                        helpers.fakeClear(input);
                        helpers.fakeTypingOut('one');
                        input.blur();
                        expect(input.value).to.equal('one');
                    });

                    it('and types "jibberish", then blurs input, should see "one hundred" in input', function () {
                        helpers.pressKey(AJS.keyCode.ENTER);
                        helpers.fakeClear(input);
                        helpers.fakeTypingOut('jibberish');
                        input.blur();
                        expect(input.value).to.equal('one hundred');
                    });
                });
            });

            describe('user types "a"', function() {
                it('should see a no suggestions message in the dropdown', function() {
                    var results = getResultsForTyping('a');

                    expect(results.length).to.equal(1);
                    expect(singleSelect._dropdown.textContent.trim()).to.equal(AJS.I18n.getText('aui.select.no.suggestions'));
                });
            });

            // because we're not doing any word breaks... yet)!
            describe('user types "hundred"', function() {
                it('should see a no suggestions message in the dropdown???', function() {
                    var results = getResultsForTyping('hundred');

                    expect(results.length).to.equal(1);
                    expect(singleSelect._dropdown.textContent.trim()).to.equal(AJS.I18n.getText('aui.select.no.suggestions'));
                });
            });
        });
    });

    describe('Single select async', function () {
        var select;
        var dropdown;
        var server;
        var clock;
        var input;
        var numbersResponse = [{
                label: 'One',
                value: 'one'
            }, {
                label: 'Two',
                value: 'two'
            }, {
                label: 'Three',
                value: 'three'
            }];

        function getResultsAsArray () {
            var elements = dropdown.querySelectorAll('li[id]');
            var options = [];

            for (var a = 0; a < elements.length; a++) {
                options.push(elements[a].textContent);
            }

            return options;
        }

        function setUpSingleSelectAsync (response, optionHtml) {
            select = createAndSkate('<aui-select src="get-select-options">' + (optionHtml || '') + '</aui-select>');
            dropdown = getDropdownFor(select);
            input = getInputFor(select);
            server = sinon.fakeServer.create();
            clock = sinon.useFakeTimers();

            input.value = '';
            input.focus();
            helpers.respondWithJson(server, response);
        }

        afterEach(function () {
            server.restore();
            clock.restore();
        });

        describe('when the dropdown is opened', function () {
            beforeEach(function () {
                setUpSingleSelectAsync(numbersResponse);
            });

            describe('user types "a"', function () {
                it('should see a no suggestions message in the dropdown', function () {
                    helpers.fakeTypingOut('a');
                    server.respond();

                    var results = getResultsAsArray();

                    expect(results.length).to.equal(0);
                    expect(dropdown.textContent.trim()).to.equal(AJS.I18n.getText('aui.select.no.suggestions'));
                });
            });

            describe('user types "t"', function () {
                it('should show "two" and "three"', function () {
                    helpers.fakeTypingOut('t');
                    server.respond();

                    var results = getResultsAsArray();

                    expect(results.length).to.equal(2);
                    expect(results).to.include.members(['Two', 'Three']);
                });

                it('should set aria-busy to true until async result returns', function () {
                    helpers.fakeTypingOut('t');
                    expect(input.getAttribute('aria-busy')).to.equal('true');
                    server.respond();
                    expect(input.getAttribute('aria-busy')).to.equal('false');
                });
            });

            describe('user types "th"', function () {
                it('should show "three"', function () {
                    helpers.fakeTypingOut('th');
                    server.respond();

                    var results = getResultsAsArray();

                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['Three']);
                });
            });

            describe('user types while a previous request is still loading', function () {
                var tServer;
                var thServer;

                beforeEach(function () {
                    setUpSingleSelectAsync(numbersResponse, '<aui-option>One</aui-option>');

                    tServer = sinon.fakeServer.create();
                    thServer = sinon.fakeServer.create();

                    helpers.respondWithJson(tServer, [{ label: 'Two' }, { label: 'Three' }]);
                    helpers.respondWithJson(thServer, [{ label: 'Three' }]);
                });

                it('types: t, h', function () {
                    var results;

                    helpers.fakeTypingOut('t');
                    helpers.fakeTypingOut('h');

                    thServer.respond();
                    results = getResultsAsArray();
                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['Three']);

                    tServer.respond();
                    results = getResultsAsArray();
                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['Three']);
                });

                it('should maintain aria-busy as true until final response returns', function () {
                    helpers.fakeTypingOut('t');
                    helpers.fakeTypingOut('h');
                    expect(input.getAttribute('aria-busy')).to.equal('true');
                    tServer.respond();
                    expect(input.getAttribute('aria-busy')).to.equal('true');
                    thServer.respond();
                    expect(input.getAttribute('aria-busy')).to.equal('false');
                });

                it('types: t, backspace', function () {
                    helpers.fakeTypingOut('t');
                    helpers.pressKey('escape');

                    tServer.respond();
                    expect(getResultsAsArray().length).to.equal(0);
                });

                it('types: t, h, backspace', function () {
                    var results;

                    helpers.fakeTypingOut('t');
                    helpers.fakeTypingOut('h');
                    helpers.fakeBackspace();

                    thServer.respond();
                    results = getResultsAsArray();
                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['Three']);

                    tServer.respond();
                    results = getResultsAsArray();
                    expect(results.length).to.equal(1);
                    expect(results).to.include.members(['Three']);
                });

                it('types: t, h, backspace, h', function () {
                    helpers.fakeTypingOut('t');
                    helpers.fakeTypingOut('h');
                    helpers.fakeBackspace();
                    helpers.fakeTypingOut('h');

                    thServer.respond();
                    expect(getResultsAsArray().length).to.equal(1);

                    tServer.respond();
                    expect(getResultsAsArray().length).to.equal(1);
                });

                it('types: t, h, backspace, escape', function () {
                    helpers.fakeTypingOut('t');
                    helpers.fakeTypingOut('h');
                    helpers.fakeBackspace();
                    helpers.fakeClear();

                    thServer.respond();
                    expect(getResultsAsArray().length).to.equal(0);

                    tServer.respond();
                    expect(getResultsAsArray().length).to.equal(0);
                });
            });
        });

        describe('with sync default values', function () {
            var response = [{
                label: 'Aquarium',
                value: 'Aquarium'
            }, {
                label: 'Aquatic',
                value: 'Aquatic'
            }, {
                label: 'Aqua',
                value: 'Aqua'
            }];

            beforeEach(function () {
                setUpSingleSelectAsync(response, '<aui-option>Apple</aui-option><aui-option>Banana</aui-option>');
            });

            describe('types: a', function () {
                beforeEach(function() {
                    input.focus();
                    helpers.pressKey(AJS.keyCode.DOWN);
                    helpers.fakeTypingOut('a');
                });

                it('should provide a message for assistive div when async response returns with more suggestions', function () {
                    expect(select.querySelector('.aui-select-status').innerHTML).to.not.be.defined;
                    server.respond();
                    expect(select.querySelector('.aui-select-status').innerHTML).to.equal(AJS.I18n.getText('aui.select.new.suggestions'));
                });
            });

            describe('types: b', function () {
                beforeEach(function() {
                    helpers.fakeTypingOut('b');
                });

                it('should not provide a message for assistive div when async response returns with no suggestions', function () {
                    expect(select.querySelector('.aui-select-status').innerHTML).to.not.be.defined;
                    server.respond();
                    expect(select.querySelector('.aui-select-status').innerHTML).to.not.be.defined;
                });
            });

            describe('types: a, q, backspace, backspace before server returns', function () {
                beforeEach(function() {
                    input.focus();
                    helpers.fakeTypingOut('a');
                    // Backspace and wait for event to register.
                    helpers.fakeBackspace();
                    clock.tick(1);
                });

                it('should only append one set of "Apple" and "Banana"', function () {
                    server.respond();
                    expect(getResultsAsArray().length).to.equal(2);
                });
            });
        });

        describe('with images', function () {
            var imagesResponse = [{
                label: 'Blue',
                value: 'blue',
                'img-src': 'blue.png'
            }, {
                label: 'Red',
                value: 'red',
                'img-src': 'red.png'
            }, {
                label: 'Green',
                value: 'green',
                'img-src': 'green.png'
            }];

            beforeEach(function () {
                setUpSingleSelectAsync(imagesResponse, '<aui-option img-src="yellow.png">Yellow</aui-option>');
            });

            describe('types: y', function () {
                beforeEach(function() {
                    helpers.fakeTypingOut('y');
                });

                it('an image is present in the dropdown list', function () {
                    var image = $(dropdown).find('img')[0];
                    expect(image).to.be.an.instanceof(HTMLElement);
                    expect(image.getAttribute('src')).to.equal('yellow.png');
                });
            });

            describe('types: r', function () {
                function getBackgroundImageSource(input) {
                    var backgroundImage = $(input).css('background-image').replace(/url\(|\)/gi, '');
                    return backgroundImage && backgroundImage.substring(backgroundImage.lastIndexOf('/') + 1);
                }

                beforeEach(function() {
                    helpers.fakeTypingOut('r');
                    server.respond();
                });

                it('an image is present in the dropdown list', function () {
                    var image = $(dropdown).find('img')[0];
                    expect(image).to.be.an.instanceof(HTMLElement);
                    expect(image.getAttribute('src')).to.equal('red.png');
                });

                describe('and the first result is clicked', function () {
                    beforeEach(function () {
                        helpers.mousedown(dropdown.querySelector('li'));
                    });

                    it('then the input is preceded by an image', function () {
                        expect(getBackgroundImageSource(input)).to.equal('red.png');
                    });
                });
            });
        });
    });
});
