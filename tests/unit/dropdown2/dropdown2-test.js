/* jshint expr:true */
define([
    '../../helpers/all.js',
    './dropdown2-test-templates.js',
    'jquery',
    'aui/internal/skate',
    'aui/internal/browser',
    'dropdown2',
    'soy/dropdown2'
], function(
    helpers,
    templates,
    $,
    skate,
    browser
) {
    'use strict';

    describe('Dropdown2', function () {
        function pressKey (keyCode, modifiers) {
            var e = $.Event('keydown');
            modifiers = modifiers || {};
            e.keyCode = keyCode;
            e.ctrlKey = !!modifiers.control;
            e.shiftKey = !!modifiers.shift;
            e.altKey = !!modifiers.alt;
            e.metaKey = !!modifiers.meta;
            $(document.activeElement).trigger(e);
        }

        function click (element) {
            $(element).click();
        }

        function hover (element) {
            var hoverEvents = ['mouseenter','mouseover','mousemove'];
            $.each(hoverEvents, function(i, name) {
                var e = $.Event(name);
                $(element).trigger(e);
            });
        }

        function isVisible (element) {
            var $element = $(element);
            return $element.is(':visible');
        }

        function isActive (element) {
            var $element = $(element);
            return $element.is('.active.aui-dropdown2-active');
        }

        // Dropdown2 - Construction
        // ------------------------
        //
        // Test the construction of dropdowns.
        // Checking basic state, and looking for race conditions or order of operations problems.
        //
        describe('Construction -', function () {
            var $trigger;
            var $dropdown;

            beforeEach(function () {
                $trigger = $('<button data-aui-trigger="toggle" aria-controls="dd22" ></button>');
                $dropdown = $('<div id="dd22" data-aui-dropdown2=""></div>');
            });

            afterEach(function () {
                helpers.removeLayers();
            });

            it('via data-aui-dropdown2 attribute', function() {
                skate.init($dropdown);
                expect($dropdown.attr('resolved')).to.equal('');
                expect($dropdown.is('.aui-dropdown2')).to.be.true;
            });

            it('via aui-dropdown2 class', function() {
                $dropdown.removeAttr('data-aui-dropdown2');
                $dropdown.addClass('aui-dropdown2');
                skate.init($dropdown);
                expect($dropdown.attr('resolved')).to.equal('');
            });

            it('hides dropdown upon construction', function() {
                skate.init($dropdown);
                expect(isVisible($dropdown)).to.be.false;
                expect($dropdown.attr('aria-hidden')).to.equal('true');
            });
        });

        describe('lazy construction', function () {
            var clock;
            var $trigger;
            var $dropdown;

            function click($el) {
                $el.trigger('mousedown');
                $el.click();
                $el.trigger('mouseup');
            }

            beforeEach(function () {
                clock = sinon.useFakeTimers();
                $trigger = $('<a></a>');
                $dropdown = $('<div id="lazy-dropdown" data-aui-dropdown2=""></div>');

                AJS.$('#test-fixture').append($trigger);
                AJS.$('#test-fixture').append($dropdown);
                skate.init($dropdown); //only skate.init $dropdown and not $trigger as we're testing the synchronous initialisation of the trigger

            });

            afterEach(function () {
                helpers.removeLayers();
                clock = sinon.restore();
            });

            it('trigger is not exapndable before attributes are added', function () {
                click($trigger);
                expect($trigger.attr('aria-expanded')).to.not.equal('true');
            });

            it('dropdown is not visible before attributes are added', function () {
                click($trigger);
                expect($dropdown.is(':visible')).to.be.false;
            });

            describe('after attributes are lazily added', function () {
                beforeEach(function () {
                    $trigger.addClass('aui-dropdown2-trigger');
                    $trigger.attr('aria-owns', 'lazy-dropdown');
                    $trigger.attr('data-aui-trigger', 'toggle');
                });

                describe('followed by clicking on the trigger', function () {
                    beforeEach(function() {
                        click($trigger);
                    });

                    it('expands the trigger', function () {
                        expect($trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('makes the dropdown visible', function () {
                        expect($dropdown.is(':visible')).to.be.true;
                    });

                    it('puts functions on the trigger element', function () {
                        expect($trigger[0].isEnabled).to.be.a('function');
                    });
                });
            });
        });

        var LegacyDropdown = function() {
            var idPrefix = AJS.id('test-legacy-dropdown');

            var $trigger = AJS.$('<a id="' + idPrefix + '-trigger" href="#dropdown2-trigger" aria-owns="' + idPrefix + '-menu" aria-haspopup="true" class="aui-dropdown2-trigger">Example dropdown</a>');
            var $dropdown = AJS.$('<div class="aui-dropdown2 aui-style-default" id="' + idPrefix + '-menu"></div>');

            var testComponent = {
                getElement: function(id) {
                    return this.$elements.find('#' + idPrefix + id);
                },
                $trigger: $trigger,
                $dropdown: $dropdown,
                $elements: $trigger.add($dropdown),
                initialise: function ($parent) {
                    this.$elements.appendTo($parent || AJS.$('#test-fixture'));
                    this.$elements.each(function() {
                        skate.init(this);
                    });
                },
                addPlainSection: function() {
                    $('#' + idPrefix + 'section1').remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.plainSection
                    );
                },

                addPlainSection2: function () {
                    $('#' + idPrefix + 'section2').remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.plainSection2
                    );
                },

                addHiddenSection: function() {
                    $('#' + idPrefix + 'section3').remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.hiddenSection
                    );
                },

                addInteractiveSection: function() {
                    $('#' + idPrefix + 'section4').remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.interactiveSection
                    );
                },

                addRadioSection: function () {
                    $('#' + idPrefix + 'section2').remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.radioSection
                    );
                },

                addCheckboxSection: function () {
                    var id = idPrefix + '-checkbox-section';
                    $('#' + id).remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.checkboxSection
                    );
                },

                addSubmenuSection: function () {
                    var id = idPrefix + '-submenu-section';
                    $('#' + id).remove();
                    $dropdown.append(
                        templates(idPrefix).legacy.submenuSection
                    );

                    /*We need to return all of these elements rather than use the getElement() function, as the layers
                    will not be nested once the layers are placed at the end of the DOM*/
                    return {
                        $menu2Dropdown: this.getElement('dd2-menu-2'),
                        $menu1Child1: this.getElement('dd2-menu-1-child-1'),
                        $menu2Trigger: this.getElement('dd2-menu-1-child-2'),
                        $menu3Dropdown: this.getElement('dd2-menu-3'),
                        $menu3Trigger: this.getElement('dd2-menu-2-child-2'),
                        $menu2Child1: this.getElement('dd2-menu-2-child-1')
                    };
                }
            };

            return testComponent;
        };

        var AccessibleDropdown = function() {
            var idPrefix = AJS.id('test-accessible-dropdown');
            var triggerId = idPrefix + '-trigger';
            var menuId = idPrefix + '-menu';


            var $trigger = AJS.$(aui.dropdown2.trigger({
                    id: triggerId,
                    text: 'Example dropdown',
                    menu: {
                        id: menuId
                    }
                }));

            var $dropdown = AJS.$(aui.dropdown2.contents({
                id: menuId
            }));

            var testComponent = {
                getElement: function(id) {
                    return this.$elements.find('#' + idPrefix + id);
                },
                $trigger: $trigger,
                $dropdown: $dropdown,
                $elements: $trigger.add($dropdown),
                initialise: function ($parent) {
                    this.$elements.appendTo($parent || AJS.$('#test-fixture'));
                    this.$elements.each(function() {
                        skate.init(this);
                    });
                },

                addPlainSection: function() {
                    $('#' + idPrefix + '-link-section').remove();
                    $dropdown.children('div').append(
                        templates(idPrefix).accessible.plainSection
                    );
                },

                addPlainSection2: function() {
                    $('#' + idPrefix + '-link-section-2').remove();
                    $dropdown.children('div').append(
                        templates(idPrefix).accessible.plainSection2
                    );
                },

                addHiddenSection: function() {
                    var id = idPrefix + '-hidden-section';
                    $('#' + id).remove();
                    $dropdown.children('div').append(
                        templates(idPrefix).accessible.hiddenSection
                    );
                },

                addInteractiveSection: function() {
                    var id = idPrefix + '-interactive-section';
                    $('#' + id).remove();

                    $dropdown.children('div').append(
                        templates(idPrefix).accessible.interactiveSection
                    );
                },

                addRadioSection: function () {
                    var id = idPrefix + '-radio-section';
                    $('#' + id).remove();

                    $dropdown.children('div').append(
                        templates(idPrefix).accessible.radioSection
                    );
                },

                addCheckboxSection: function () {
                    var id = idPrefix + '-checkbox-section';
                    $('#' + id).remove();
                    $dropdown.children('div').append(
                        templates(idPrefix).accessible.checkboxSection
                    );
                },

                addSubmenuSection: function () {
                    $dropdown.children('div').append(aui.dropdown2.itemGroup({
                        items: [
                            {text: 'Dummy item 1', id: idPrefix + 'dd2-menu-1-child-1'},
                            {text: 'Open submenu level 1', submenuTarget: idPrefix + 'dd2-menu-2', id: idPrefix + 'dd2-menu-1-child-2'}
                        ]
                    }));

                    var $menu2 = $(aui.dropdown2.contents({
                        id: idPrefix + 'dd2-menu-2',
                        content: aui.dropdown2.itemGroup({
                            items: [
                                {text: 'Dummy item 2', id: idPrefix + 'dd2-menu-2-child-1'},
                                {text: 'Open submenu level 2', submenuTarget: idPrefix + 'dd2-menu-3', id: idPrefix + 'dd2-menu-2-child-2'}
                            ]
                        })
                    }));

                    this.$elements = this.$elements.add($menu2);

                    var $menu3 = $(aui.dropdown2.contents({
                        id: idPrefix + 'dd2-menu-3',
                        content: aui.dropdown2.itemGroup({
                            items: [
                                {text: 'Final level'}
                            ]
                        })
                    }));

                    this.$elements = this.$elements.add($menu3);

                    /*We need to return all of these elements rather than use the getElement() function, as the layers
                     will not be nested once the layers are placed at the end of the DOM*/
                    return {
                        $menu2Dropdown: this.getElement('dd2-menu-2'),
                        $menu1Child1: this.getElement('dd2-menu-1-child-1'),
                        $menu2Trigger: this.getElement('dd2-menu-1-child-2'),
                        $menu3Dropdown: this.getElement('dd2-menu-3'),
                        $menu3Trigger: this.getElement('dd2-menu-2-child-2'),
                        $menu2Child1: this.getElement('dd2-menu-2-child-1')
                    };
                }
            };

            return testComponent;
        };

        describe('with voiceover', function () {
            beforeEach(function () {
                sinon.stub(browser, 'supportsVoiceOver').returns(true);
            });

            afterEach(function () {
                browser.supportsVoiceOver.restore();
            });

            runAccessibleAndLegacyMarkupTests();
        });

        describe('without voiceover', function () {
            beforeEach(function () {
                sinon.stub(browser, 'supportsVoiceOver').returns(false);
            });

            afterEach(function () {
                browser.supportsVoiceOver.restore();
            });

            runAccessibleAndLegacyMarkupTests();
        });


        function runAccessibleAndLegacyMarkupTests () {
            describe('with legacy markup pattern', function () {
                runDropdown2UnitTests(LegacyDropdown);
            });

            describe('with new, accessible markup pattern', function () {
                runDropdown2UnitTests(AccessibleDropdown);
            });
        }


        function runDropdown2UnitTests(Dropdown) {
            // Dropdown2 - Single Dropdown Environment
            // ---------------------------------------
            //
            // Create the basic dropdown test environment.
            // There is only one dropdown and one trigger in this environment.
            // The assertion of how dropdowns behave with other dropdowns and layers comes later.
            //
            describe('and one dropdown+trigger', function () {
                var clock;
                var $hideout;
                var singleDropdown;

                beforeEach(function () {
                    clock = sinon.useFakeTimers();

                    singleDropdown = new Dropdown();
                    $hideout = $('<div id="hideout"></div>');
                    AJS.$('#test-fixture').append($hideout);
                });

                afterEach(function () {
                    clock.restore();

                    $('.aui-dropdown2').remove();
                    $('.aui-dropdown2-trigger').remove();
                    $hideout.remove();

                    singleDropdown = null;

                    helpers.removeLayers();
                });

                function invokeTrigger($el) {
                    $el.trigger('aui-button-invoke');
                    clock.tick(100);
                }

                describe('with a link section,', function () {
                    beforeEach(function() {
                        singleDropdown.addPlainSection();
                        singleDropdown.initialise();
                    });

                    // Dropdown2 - Trigger
                    // -------------------
                    //
                    // Test opening and closing a dropdown via its trigger,
                    // and the expected state of both the trigger and dropdown.
                    //
                    describe('on the trigger element', function () {
                        beforeEach(function () {
                            expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                            expect(isActive(singleDropdown.$trigger)).to.be.false;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('false');
                        });

                        it('firing aui-button-invoke event opens dropdown', function () {
                            invokeTrigger(singleDropdown.$trigger);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                        });

                        it('firing aui-button-invoke hides dropdown if dropdown was open', function () {
                            invokeTrigger(singleDropdown.$trigger);
                            invokeTrigger(singleDropdown.$trigger);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                            expect(isActive(singleDropdown.$trigger)).to.be.false;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('false');
                        });

                        it('pressing Spacebar opens the dropdown', function () {
                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.SPACE);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                        });

                        it('pressing Enter opens the dropdown', function () {
                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.ENTER);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                        });

                        it('pressing Up Arrow key opens the dropdown', function () {
                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.UP);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                        });

                        it('pressing Down Arrow key opens the dropdown', function () {
                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.DOWN);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                        });

                        it('clicking opens the dropdown', function () {
                            click(singleDropdown.$trigger);

                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                        });

                        it('cannot open dropdown when disabled', function () {
                            singleDropdown.$trigger.attr('aria-disabled', 'true');

                            click(singleDropdown.$trigger);

                            expect(isActive(singleDropdown.$trigger)).to.be.false;
                            expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('false');
                            expect(document.location.hash).to.not.equal(singleDropdown.$trigger.attr('href'));
                        });

                        it('clicking adds aui-dropdown2-active and active classes to trigger', function () {
                            click(singleDropdown.$trigger);

                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                        });

                        it('aui-button-invoke adds aui-dropdown2-active and active classes to trigger', function () {
                            invokeTrigger(singleDropdown.$trigger);

                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                        });
                    });

                    describe('on the dropdown element', function () {
                        it('aui-dropdown2-show event is fired when it is shown by click', function () {
                            var spy = sinon.spy();
                            singleDropdown.$dropdown.on('aui-dropdown2-show', spy);

                            click(singleDropdown.$trigger);

                            spy.should.have.been.calledOnce;
                        });

                        it('aui-dropdown2-show event is fired when it is shown by invoke', function () {
                            var spy = sinon.spy();
                            singleDropdown.$dropdown.on('aui-dropdown2-show', spy);

                            invokeTrigger(singleDropdown.$trigger);

                            spy.should.have.been.calledOnce;
                        });

                        it('aui-dropdown2-hide is fired when it is hidden by click', function () {
                            var spy = sinon.spy();
                            singleDropdown.$dropdown.on('aui-dropdown2-hide', spy);

                            clock.tick(1000);
                            click(singleDropdown.$trigger);
                            clock.tick(1000);
                            click(singleDropdown.$trigger);
                            clock.tick(1000);

                            spy.should.have.been.calledOnce;
                        });

                        it('aui-dropdown2-hide is fired when it is closed by invoke', function () {
                            var spy = sinon.spy();
                            singleDropdown.$dropdown.on('aui-dropdown2-hide', spy);

                            click(singleDropdown.$trigger);
                            invokeTrigger(singleDropdown.$trigger);

                            spy.should.have.been.calledOnce;
                        });

                        it('aria-hidden set to false when opened', function () {
                            click(singleDropdown.$trigger);
                            expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('false');
                        });

                        it('aria-hidden set to true when hidden', function () {
                            click(singleDropdown.$trigger);
                            click(singleDropdown.$trigger);
                            expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('true');
                        });

                        it('closes when Escape is pressed', function () {
                            click(singleDropdown.$trigger);
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('true');
                        });
                    });

                    // Dropdown2 - API
                    // ---------------
                    //
                    // Test the functions present on a dropdown2 component element
                    //
                    describe('Dropdown Element API -', function () {
                        it('show() will open dropdown', function () {
                            singleDropdown.$dropdown[0].show();
                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        });

                        it('hide() will close dropdown', function () {
                            singleDropdown.$dropdown[0].show();
                            singleDropdown.$dropdown[0].hide();
                            expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                        });

                        it('isVisible() returns false when hidden on load', function () {
                            expect(singleDropdown.$dropdown[0].isVisible()).to.be.false;
                        });

                        it('isVisible() returns true when visible', function () {
                            singleDropdown.$dropdown[0].show();
                            expect(singleDropdown.$dropdown[0].isVisible()).to.be.true;
                        });

                        it('isVisible() returns false when hidden after hide', function () {
                            singleDropdown.$dropdown[0].hide();
                            expect(singleDropdown.$dropdown[0].isVisible()).to.be.false;
                        });
                    });

                    // Dropdown 2 - DOM
                    // ----------------
                    //
                    // Test configuring a dropdown element's resting place, etc.
                    //
                    describe('DOM -', function () {
                        it('returned to original location if data-dropdown2-hide-location is not specified', function () {
                            var originalParent = singleDropdown.$dropdown.parent()[0];

                            click(singleDropdown.$trigger);
                            clock.tick(100);
                            click(singleDropdown.$trigger);
                            clock.tick(100);

                            expect(singleDropdown.$dropdown.parent()[0]).to.equal(originalParent);
                            expect($hideout.find(singleDropdown.$dropdown).length).to.equal(0);
                        });

                        it('specifying the data-dropdown2-hide-location works properly when dropdown is hidden', function () {
                            singleDropdown.$trigger.attr('data-dropdown2-hide-location', 'hideout');
                            var originalParent = singleDropdown.$dropdown.parent()[0];

                            click(singleDropdown.$trigger);
                            clock.tick(100);
                            click(singleDropdown.$trigger);
                            clock.tick(100);

                            expect(singleDropdown.$dropdown.parent()[0]).to.not.equal(originalParent);
                            expect($hideout.find(singleDropdown.$dropdown).length).to.equal(1);
                        });
                    });

                    // Dropdown2 - Items
                    // -----------------
                    //
                    // Test navigating and interacting with items in the dropdown
                    //
                    describe('Items -', function () {
                        beforeEach(function() {
                            singleDropdown.addPlainSection();
                            singleDropdown.addHiddenSection();
                            singleDropdown.addInteractiveSection();
                            singleDropdown.initialise();
                        });

                        it('first item NOT active when dropdown opened by mouse', function() {
                            var $i1 = singleDropdown.getElement('item1');

                            click(singleDropdown.$trigger);

                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(isActive($i1)).to.be.false;
                        });

                        it('first item is focused when dropdown opened by keyboard (a11y)', function() {
                            var $i1 = singleDropdown.getElement('item1');

                            singleDropdown.$trigger.focus();
                            expect(document.activeElement).to.equal(singleDropdown.$trigger[0]);

                            pressKey(AJS.keyCode.SPACE);
                            expect(document.activeElement).to.equal($i1[0]);
                        });

                        it('can navigate items using down arrow key', function() {
                            var $i1 = singleDropdown.getElement('item1');
                            var $i2 = singleDropdown.getElement('item2');

                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                            pressKey(AJS.keyCode.DOWN);

                            expect(isActive($i1)).to.be.false;
                            expect(isActive($i2)).to.be.true;
                        });

                        it('navigated correctly using keys with hidden items', function() {
                            var $i1 = singleDropdown.getElement('item1'),
                                $i2 = singleDropdown.getElement('item2'),
                                $i3 = singleDropdown.getElement('item3'),
                                $i4 = singleDropdown.getElement('item4');

                            $i1.parent().addClass('hidden');
                            $i3.parent().addClass('hidden');

                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                            expect(isActive($i1)).to.be.false;
                            expect(isActive($i2)).to.be.true;

                            pressKey(AJS.keyCode.DOWN);

                            expect(isActive($i2)).to.be.false;
                            expect(isActive($i3)).to.be.false;
                            expect(isActive($i4)).to.be.true;
                        });

                        it('navigated correctly using keys with hidden items that were added after opened', function() {
                            var $i1 = singleDropdown.getElement('item1'),
                                $i2 = singleDropdown.getElement('item2'),
                                $i3 = singleDropdown.getElement('item3'),
                                $i4 = singleDropdown.getElement('item4');

                            $i1.parent().addClass('hidden');

                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.ENTER);

                            $i3.parent().addClass('hidden');
                            pressKey(AJS.keyCode.DOWN);

                            expect(isActive($i2)).to.be.false;
                            expect(isActive($i3)).to.be.false;
                            expect(isActive($i4)).to.be.true;
                        });

                        it('can be focused with the keyboard even when disabled (a11y)', function() {
                            var $i2 = singleDropdown.getElement('item2');
                            var $i3 = singleDropdown.getElement('item3');
                            var $i4 = singleDropdown.getElement('item4');

                            $i2.attr('aria-disabled', true);
                            $i3.attr('aria-disabled', true);

                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                            pressKey(AJS.keyCode.DOWN);

                            expect(isActive($i4)).to.be.false;
                            expect(isActive($i2)).to.be.true;
                        });

                        it('can be activated by clicking', function() {
                            var spy = sinon.spy();
                            var $i1 = singleDropdown.getElement('item1').attr('href','#actionable');

                            $i1.on('click', spy);
                            click(singleDropdown.$trigger);
                            click($i1);

                            spy.should.have.been.calledOnce;
                            expect(spy.getCall(0).args[0].isDefaultPrevented()).to.be.false;
                        });

                        it('cannot activate disabled items', function() {
                            var spy = sinon.spy();
                            var $i1 = singleDropdown.getElement('item1').attr('href', '#shouldnt-be-actionable');
                            $i1.attr('aria-disabled', true);

                            $i1.on('click', spy);
                            click(singleDropdown.$trigger);
                            click($i1);

                            spy.should.have.been.calledOnce;
                            expect(spy.getCall(0).args[0].isDefaultPrevented()).to.be.true;
                        });

                        it('clicking normal dropdown items should close dropdown', function () {
                            var $ir3 = singleDropdown.getElement('iradio3-unchecked');

                            click(singleDropdown.$trigger);

                            click($ir3);
                            clock.tick(100);

                            expect(isActive(singleDropdown.$trigger)).to.be.false;
                            expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                        });

                        it('clicking a dropdown item with the \'aui-dropdown2-interactive\' class doesn\'t hide the dropdown', function() {
                            var $ir1 = singleDropdown.getElement('iradio1-interactive-checked');

                            click(singleDropdown.$trigger);

                            click($ir1);
                            clock.tick(100);

                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        });

                        it('clicking a dropdown item with the legacy \'interactive\' class doesn\'t hide the dropdown', function() {
                            var $ir2 = singleDropdown.getElement('iradio2-interactive-unchecked');

                            click(singleDropdown.$trigger);

                            click($ir2);
                            clock.tick(100);

                            expect(isActive(singleDropdown.$trigger)).to.be.true;
                            expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        });

                        it('are reverted to inactive when the dropdown is closed', function() {
                            var $i3 = singleDropdown.getElement('item3');

                            singleDropdown.$trigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                            pressKey(AJS.keyCode.DOWN);
                            pressKey(AJS.keyCode.DOWN);

                            expect(isActive($i3)).to.be.true;

                            pressKey(AJS.keyCode.ESCAPE);

                            expect(isActive($i3)).to.be.false;
                        });
                    });
                });


                // Dropdown2 - Special Items
                // -------------------------
                //
                // Test interaction with special dropdown item types, like
                // checkboxes and radio buttons.
                //
                describe('with checkbox and hidden items; clicking the trigger', function () {
                    beforeEach(function() {
                        singleDropdown.addCheckboxSection();
                        singleDropdown.addHiddenSection();
                        singleDropdown.initialise();

                        click(singleDropdown.$trigger);
                    });

                    it('adds aui-dropdown2-checked on click checkboxes', function() {
                        var $c1 = singleDropdown.getElement('check1-unchecked');
                        click($c1);
                        expect($c1.is('.checked.aui-dropdown2-checked')).to.be.true;
                    });

                    it('toggles checked to unchecked on checkboxes', function() {
                        var $c1 = singleDropdown.getElement('check1-unchecked');
                        var $c2 = singleDropdown.getElement('check2-checked');

                        click($c1);
                        click($c2);

                        expect($c1.is('.checked.aui-dropdown2-checked')).to.be.true;
                        expect($c2.is('.checked,.aui-dropdown2-checked')).to.be.false;
                    });

                    it('fires aui-dropdown2-item-check on checkboxes', function() {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-item-check', spy);

                        var $c1 = singleDropdown.getElement('check1-unchecked'),
                            $c2 = singleDropdown.getElement('check2-checked'),
                            $c3 = singleDropdown.getElement('check3-unchecked');

                        click($c1);
                        click($c2);
                        click($c3);

                        spy.should.have.been.calledTwice;
                    });

                    it('fires aui-dropdown2-item-uncheck on checkboxes', function() {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-item-uncheck', spy);

                        var $c1 = singleDropdown.getElement('check1-unchecked'),
                            $c2 = singleDropdown.getElement('check2-checked'),
                            $c3 = singleDropdown.getElement('check3-unchecked');

                        click($c1);
                        click($c2);
                        click($c3);

                        spy.should.have.been.calledOnce;
                    });

                    it('cannot check hidden, disabled checkboxes', function() {
                        var $h1 = singleDropdown.getElement('hidden1-unchecked-disabled');
                        var $h2 = singleDropdown.getElement('hidden2-checked');

                        click($h1);
                        click($h2);

                        expect($h1.is('.checked.aui-dropdown2-checked')).to.be.false;
                        expect($h2.is('.aui-dropdown2-checked')).to.be.true;
                    });
                });

                describe('with radio buttons; clicking the trigger', function () {
                    beforeEach(function() {
                        singleDropdown.addRadioSection();
                        singleDropdown.initialise();
                        click(singleDropdown.$trigger);
                    });

                    it('and clicking radio buttons adds aui-dropdown2-checked', function() {
                        var $r3 = singleDropdown.getElement('radio3-unchecked');
                        click($r3);
                        expect($r3.is('.checked.aui-dropdown2-checked')).to.be.true;
                    });

                    it('toggles checked to unchecked on radio buttons', function() {
                        var $r1 = singleDropdown.getElement('radio1-unchecked');
                        var $r2 = singleDropdown.getElement('radio2-checked');

                        click($r1);

                        expect($r1.is('.checked.aui-dropdown2-checked')).to.be.true;
                        expect($r2.is('.checked,.aui-dropdown2-checked')).to.be.false;
                    });

                    it('fires aui-dropdown2-item-check on radios', function() {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-item-check', spy);

                        var $r1 = singleDropdown.getElement('radio1-unchecked');
                        var $r2 = singleDropdown.getElement('radio2-checked');
                        var $r3 = singleDropdown.getElement('radio3-unchecked');

                        click($r2);
                        click($r1);
                        click($r3);

                        spy.should.have.been.calledTwice;
                    });

                    it('fires aui-dropdown2-item-uncheck on radios', function() {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-item-check', spy);

                        var $r1 = singleDropdown.getElement('radio1-unchecked');
                        var $r2 = singleDropdown.getElement('radio2-checked');
                        var $r3 = singleDropdown.getElement('radio3-unchecked');

                        click($r1);
                        click($r2);
                        click($r3);

                        spy.should.have.been.calledThrice;
                    });
                });

                // Dropdown2 - Submenus
                // --------------------
                //
                // Test environment setup for dropdowns with submenus.
                // Test the basic interactions with dropdown submenus.
                //
                describe('which has a submenu', function () {
                    var clock;
                    var submenus;
                    var $firstMenuTrigger;
                    var $secondMenuTrigger;
                    var $thirdMenuTrigger;

                    beforeEach(function () {
                        submenus = singleDropdown.addSubmenuSection();
                        singleDropdown.initialise();
                        clock = sinon.useFakeTimers();

                        $firstMenuTrigger = singleDropdown.$trigger;
                        $secondMenuTrigger = submenus.$menu2Trigger;
                        $thirdMenuTrigger = submenus.$menu3Trigger;
                    });

                    afterEach(function () {
                        clock.restore();
                        helpers.removeLayers();
                    });

                    function countOpenDropdowns () {
                        return $('.aui-dropdown2[aria-hidden=false]').length;
                    }

                    it('can be opened with aui-button-invoke', function() {
                        expect(countOpenDropdowns()).to.equal(0);

                        invokeTrigger($firstMenuTrigger);
                        invokeTrigger($secondMenuTrigger);

                        expect(countOpenDropdowns()).to.equal(2);
                    });

                    it('will focus first item in submenu if opened via keyboard (a11y)', function() {
                        $firstMenuTrigger.focus();
                        clock.tick(100);
                        pressKey(AJS.keyCode.SPACE);
                        clock.tick(100);
                        $secondMenuTrigger.focus();
                        clock.tick(100);
                        pressKey(AJS.keyCode.SPACE);
                        clock.tick(100);

                        expect(document.activeElement).to.equal(submenus.$menu2Child1[0]);
                    });

                    it('can be opened via the right arrow key', function() {
                        $firstMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        $secondMenuTrigger.focus();
                        pressKey(AJS.keyCode.RIGHT);
                        expect(countOpenDropdowns()).to.equal(2);
                        pressKey(AJS.keyCode.DOWN);
                        pressKey(AJS.keyCode.RIGHT);
                        expect(countOpenDropdowns()).to.equal(3);
                    });

                    it('will not open when pressing right arrow on a non trigger item', function() {
                        $firstMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        pressKey(AJS.keyCode.RIGHT);
                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('will not open when pressing the up arrow on a trigger item', function() {
                        $firstMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        $secondMenuTrigger.focus();
                        pressKey(AJS.keyCode.UP);
                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('will not open when pressing the down arrow on a trigger item', function() {
                        $firstMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        $secondMenuTrigger.focus();
                        pressKey(AJS.keyCode.DOWN);
                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('can be opened by hovering their trigger', function () {
                        var $nonTriggerInFirstMenu = submenus.$menu1Child1;

                        click($firstMenuTrigger);
                        hover($secondMenuTrigger);
                        clock.tick(100);
                        hover($thirdMenuTrigger);
                        clock.tick(100);

                        expect(countOpenDropdowns()).to.equal(3);

                        hover($nonTriggerInFirstMenu);
                        clock.tick(100);

                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('cannot be opened if their trigger is aria-disabled', function() {
                        $secondMenuTrigger.attr('aria-disabled', 'true');

                        click($firstMenuTrigger);

                        hover($secondMenuTrigger);
                        expect(countOpenDropdowns()).to.equal(1);

                        click($secondMenuTrigger);
                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('cannot be opened if their triggers have the disabled or aui-dropdown2-disabled class', function() {
                        $secondMenuTrigger.addClass('disabled aui-dropdown2-disabled');

                        click($firstMenuTrigger);

                        hover($secondMenuTrigger);
                        expect(countOpenDropdowns()).to.equal(1);

                        click($secondMenuTrigger);
                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('will not close top-level dropdown when pressing left arrow key in a top-level menu', function() {
                        $firstMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        pressKey(AJS.keyCode.LEFT);

                        expect(countOpenDropdowns()).to.be.at.least(1);
                    });

                    describe('and all submenus are opened with an event', function () {
                        beforeEach(function () {
                            invokeTrigger($firstMenuTrigger);
                            invokeTrigger($secondMenuTrigger);
                            invokeTrigger($thirdMenuTrigger);
                            clock.tick(100);
                        });

                        it('leave three open dropdowns', function () {
                            expect(countOpenDropdowns()).to.equal(3);
                        });

                        it('will close all nested submenus when the submenu trigger is clicked', function() {
                            click($secondMenuTrigger);
                            expect(countOpenDropdowns()).to.equal(1);
                        });

                        it('will not place focus on dropdown trigger when clicking the document to close dropdowns', function() {
                            click(document);
                            clock.tick(100);
                            expect(document.activeElement).to.not.equal($firstMenuTrigger[0]);
                        });
                    });

                    describe('and all submenus are opened with click', function () {
                        beforeEach(function () {
                            click($firstMenuTrigger);
                            click($secondMenuTrigger);
                            click($thirdMenuTrigger);
                        });

                        it('leave three open dropdowns', function () {
                            expect(countOpenDropdowns()).to.equal(3);
                        });

                        it('will all close when the document is clicked', function() {
                            click(document);
                            clock.tick(100);
                            expect(countOpenDropdowns()).to.equal(0);
                        });

                        it('will all close when root dropdown trigger is clicked', function() {
                            click($firstMenuTrigger);
                            expect(countOpenDropdowns()).to.equal(0);
                        });
                    });

                    describe('and all submenus are opened with hover', function () {
                        beforeEach(function () {
                            click($firstMenuTrigger);
                            hover($secondMenuTrigger);
                            clock.tick(100);
                            hover($thirdMenuTrigger);
                            clock.tick(100);
                        });

                        it('leave three open dropdowns', function () {
                            expect(countOpenDropdowns()).to.equal(3);
                        });

                        it('will not place focus on dropdown submenu trigger when hovering a non-trigger item in that menu', function() {
                            var $nonTriggerInFirstMenu = singleDropdown.getElement('dd2-menu-1-child-1');
                            hover($nonTriggerInFirstMenu);
                            clock.tick(100);

                            expect(countOpenDropdowns()).to.equal(1);
                            expect(document.activeElement).to.not.equal($firstMenuTrigger[0]);
                        });
                    });

                    describe('and all submenus are opened with the keyboard', function () {
                        beforeEach(function () {
                            $firstMenuTrigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                            $secondMenuTrigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                            $thirdMenuTrigger.focus();
                            pressKey(AJS.keyCode.ENTER);
                        });

                        it('leave three open dropdowns', function () {
                            expect(countOpenDropdowns()).to.equal(3);
                        });

                        it('then dropdowns can be closed from any position in a dropdown with the left arrow key', function() {

                            pressKey(AJS.keyCode.LEFT);

                            expect(countOpenDropdowns()).to.equal(2);

                            pressKey(AJS.keyCode.UP);
                            pressKey(AJS.keyCode.LEFT);

                            expect(countOpenDropdowns()).to.equal(1);
                        });

                        it('will leave their triggers active when closed by pressing Escape', function() {
                            expect(isActive($secondMenuTrigger)).to.be.true;
                            expect(isActive($thirdMenuTrigger)).to.be.true;

                            pressKey(AJS.keyCode.ESCAPE);

                            expect(isActive($thirdMenuTrigger)).to.be.true;
                            expect(isActive($secondMenuTrigger)).to.be.true;
                            expect(document.activeElement).to.equal($thirdMenuTrigger[0]);

                            pressKey(AJS.keyCode.ESCAPE);

                            expect(isActive($thirdMenuTrigger)).to.be.false;
                            expect(isActive($secondMenuTrigger)).to.be.true;
                            expect(document.activeElement).to.equal($secondMenuTrigger[0]);
                        });

                        it('will close one submenu at a time when pressing Escape', function() {
                            expect(countOpenDropdowns()).to.equal(3);
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(countOpenDropdowns()).to.equal(2);
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(countOpenDropdowns()).to.equal(1);
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(countOpenDropdowns()).to.equal(0);
                        });

                        it('will place focus on dropdown trigger when closed with Escape', function() {
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(document.activeElement).to.equal($thirdMenuTrigger[0]);
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(document.activeElement).to.equal($secondMenuTrigger[0]);
                            pressKey(AJS.keyCode.ESCAPE);
                            expect(document.activeElement).to.equal($firstMenuTrigger[0]);
                        });
                    });
                });
            });

            // Dropdown2 - Multiple Dropdown Environment
            // -----------------------------------------
            //
            // Create a dropdown test environment with multiple dropdowns.
            // This environment is for asserting that interaction a second dropdown+trigger will
            // put the first dropdown+trigger in an expected state.
            //
            describe('and multiple dropdowns', function () {
                var clock;
                var dropdown1;
                var dropdown2;

                beforeEach(function () {
                    clock = sinon.useFakeTimers();

                    dropdown1 = new Dropdown();
                    dropdown1.addPlainSection();
                    dropdown1.addPlainSection2();

                    dropdown2 = new Dropdown();
                    dropdown2.addPlainSection();
                    dropdown2.addPlainSection2();

                    var $triggerGroup = AJS.$('<ul class="aui-dropdown2-trigger-group"></ul>');
                    AJS.$('#test-fixture').append($triggerGroup);

                    dropdown1.initialise($triggerGroup);
                    $triggerGroup.append('<a href="#">Not a menu trigger</a>');
                    dropdown2.initialise($triggerGroup);
                });

                afterEach(function () {
                    clock.restore();

                    $('.aui-dropdown2').remove();
                    $('.aui-dropdown2-trigger').remove();

                    dropdown1.$elements.remove();
                    dropdown2.$elements.remove();

                    dropdown1 = null;
                    dropdown2 = null;

                    helpers.removeLayers();
                });

                it('open on click correctly one dropdown', function () {
                    click(dropdown1.$trigger);
                    clock.tick(100);

                    expect(isActive(dropdown1.$trigger)).to.be.true;
                    expect(isActive(dropdown2.$trigger)).to.be.false;
                });

                it('in aui-dropdown2-trigger-group open on click correctly multiple dropdowns', function () {
                    click(dropdown1.$trigger);
                    clock.tick(100);
                    click(dropdown2.$trigger);
                    clock.tick(100);

                    expect(isActive(dropdown1.$trigger)).to.be.false;
                    expect(isActive(dropdown2.$trigger)).to.be.true;
                });

                it('in aui-dropdown2-trigger-group open on hover after one is clicked', function () {
                    click(dropdown1.$trigger);
                    clock.tick(100);
                    hover(dropdown2.$trigger);
                    clock.tick(0);

                    expect(isActive(dropdown1.$trigger)).to.be.false;
                    expect(isActive(dropdown2.$trigger)).to.be.true;
                });

                it('in aui-dropdown2-trigger-group doesnt open on over when one not clicked', function () {
                    hover(dropdown2.$trigger);
                    clock.tick(0);

                    expect(isActive(dropdown1.$trigger)).to.be.false;
                    expect(isActive(dropdown2.$trigger)).to.be.false;
                });

                it('in aui-dropdown2-trigger-group pressing tab will focus next item in group', function () {
                    var keySpy = sinon.spy();

                    dropdown1.$trigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    pressKey(AJS.keyCode.DOWN);

                    expect(isActive(dropdown1.$trigger)).to.be.true;
                    expect(isActive(dropdown2.$trigger)).to.be.false;

                    $(document).on('keydown', keySpy);
                    pressKey(AJS.keyCode.TAB);
                    $(document).off('keydown', keySpy);

                    expect(isActive(dropdown1.$trigger)).to.be.false;
                    expect(isActive(dropdown2.$trigger)).to.be.false;
                    // I'd love to test document.activeElement, but I can't adequately fake the native behaviour such that the focus moves of its own accord in a test.
                    keySpy.should.have.been.calledOnce;
                    expect(keySpy.args[0][0].isDefaultPrevented()).to.be.false;
                });

                it('in aui-dropdown2-trigger-group pressing shift-tab will focus previous item in group', function () {
                    var keySpy = sinon.spy();

                    dropdown2.$trigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    pressKey(AJS.keyCode.DOWN);

                    expect(isActive(dropdown1.$trigger)).to.be.false;
                    expect(isActive(dropdown2.$trigger)).to.be.true;

                    $(document).one('keydown', keySpy);
                    pressKey(AJS.keyCode.TAB, { shift: true });

                    expect(isActive(dropdown1.$trigger)).to.be.false;
                    expect(isActive(dropdown2.$trigger)).to.be.false;
                    // I'd love to test document.activeElement, but I can't adequately fake the native behaviour such that the focus moves of its own accord in a test.
                    keySpy.should.have.been.calledOnce;
                    expect(keySpy.args[0][0].isDefaultPrevented()).to.be.false;
                });

                it('only allows for one trigger to have aui-dropdown2-active and active classes', function () {
                    click(dropdown1.$trigger);
                    expect(isActive(dropdown1.$trigger)).to.be.true;

                    // Open the second dropdown
                    click(dropdown2.$trigger);

                    expect(isActive(dropdown2.$trigger)).to.be.true;
                    expect(isActive(dropdown1.$trigger)).to.be.false;
                });

                describe(', one of which is has a submenu;', function () {
                    beforeEach(function () {
                        dropdown1.$dropdown.find('a').first()
                            .after(dropdown2.$dropdown);
                    });

                    it('the keybouard should only navigate items in immediate dropdown', function() {
                        dropdown1.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        expect(dropdown1.$dropdown.find('.active')[0]).to.equal(dropdown1.getElement('item1')[0]);

                        pressKey(AJS.keyCode.DOWN);
                        pressKey(AJS.keyCode.DOWN);

                        expect(dropdown1.$dropdown.find('.active')[0]).to.equal(dropdown1.getElement('item3')[0]);
                    });
                });
            });
        }

    });
});
