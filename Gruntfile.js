module.exports = function (grunt) {
    'use strict';

    var config = require('./build/configs')(grunt);

    config.jquery = grunt.option('jquery') || '1.8.3';
    config.pkg = grunt.file.readJSON('package.json');
    config.paths = {
        jsSource: 'src/js/',
        jsVendorSource: 'src/js-vendor/',
        bowerSource: 'bower_components/',
        styleSource: 'src/less/',
        cssVendorSource: 'src/css-vendor/',
        soySource: 'src/soy/',
        compiledSoySource: '.tmp/compiled-soy/',
        i18nBundle: 'src/i18n/aui.properties',
        dist: 'dist/',
        tmp: '.tmp/'
    };

    grunt.initConfig(config);

    grunt.loadTasks('build/tasks');
    grunt.loadNpmTasks('grunt-available-tasks');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-jscs');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-plato');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-text-replace');

    grunt.registerTask('default', 'Shows the available tasks.', 'availabletasks');
    grunt.registerTask('lint', 'Lints the code using JSHint and JSCS.  You can pass in a --files option to specify a file or a glob of files', ['jshint', 'jscs']);
    grunt.registerTask('test', 'Runs the unit tests.', ['shell:distI18n', 'soy-compile:core', 'requirejs-config', 'karma:cli', 'clean:tmp']);
    grunt.registerTask('test-debug', 'Runs the unit tests.', ['watch:test', 'clean:tmp']);
    grunt.registerTask('test-dist', 'Runs the unit tests with the dist', ['flatpack', 'requirejs-config', 'karma:flatpack', 'clean:tmp', 'clean:dist']);

    grunt.registerTask('build', 'Builds AUI', [
        'build-soy-and-js',
        'less:dist',
        'cssmin:dist',
        'copy:distAssets',
        'replace:version',
        'clean:postDist'
    ]);

    grunt.registerTask('build-soy-and-js', 'Builds AUI Soy and JavaScript', [
        'build-soy',
        'build-js'
    ]);

    grunt.registerTask('build-soy', 'Builds AUI Soy, copying it to the dist directory. Does not minify or concat the old flatpack', [
        'soy-compile:core',
        'build-js-amd',
        'copy:dist'
    ]);

    grunt.registerTask('build-js', 'Builds and minifies the JavaScript.', [
        'shell:distI18n',
        'build-js-amd',
        'copy:dist',
        'uglify:dist',
        'replace:version',
        'clean:postDist'
    ]);

    grunt.registerTask('build-js-amd', 'Creates AMD stubs and copies requirejs transformed dist files to the temp directory', [
        'shell:distI18n',
        'shell:amdStubs',
        'copy:requirejsTransformed',
        'requirejs:dist'
    ]);

    grunt.registerTask('build-js-fast', 'Builds AUI JavaScript, with no deprecation and minification (for devspeed)', [
        'shell:distI18n',
        'build-js-amd',
        'copy:dist',
        'replace:version',
        'clean:postDist'
    ]);
};
